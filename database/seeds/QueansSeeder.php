<?php

use Illuminate\Database\Seeder;
use App\Answer;
use App\Question;
use App\Email;
use App\User;

class QueansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@irkdvs.ru';
        $user->password = bcrypt('adminadmin');
        $user->save();


        $que = new Question();
        $que->name = 'Олег';
        $que->email = 'oleg@oleg.com';
            $email = new Email();
            $email->email = $que->email;
            $email->name = $que->name;
            $email->save();
        $que->comment =  'Сколько будет 2+2?';
        $que->status = '1';
        $que->save();
            $ans = new Answer();
            $ans->name = 'Гена';
            $ans->email = 'gena@gena.com';
                $email = new Email();
                $email->email = $ans->email;
                $email->name = $ans->name;
                $email->save();
            $ans->comment = 'Будет 4';
            $ans->status = '1';
        $que->answers()->save($ans);
            $ans = new Answer();
            $ans->name = 'Лёша';
            $ans->email = 'lex@gena.com';
                $email = new Email();
                $email->email = $ans->email;
                $email->name = $ans->name;
                $email->save();
            $ans->comment = 'Вы правы, будет 4';
            $ans->status = '1';
        $que->answers()->save($ans);

        $que = new Question();
        $que->name = 'Боря';
        $que->email = 'borya@oleg.com';
            $email = new Email();
            $email->email = $que->email;
            $email->name = $que->name;
            $email->save();
        $que->comment =  'Сколько будет 3+3?';
        $que->status = '1';
        $que->save();
            $ans = new Answer();
            $ans->name = 'Алиса';
            $ans->email = 'alice@wonderland.com';
                $email = new Email();
                $email->email = $ans->email;
                $email->name = $ans->name;
                $email->save();
            $ans->comment = 'Будет 6';
            $ans->status = '1';
        $que->answers()->save($ans);
            $ans = new Answer();
            $ans->name = 'Эвелин';
            $ans->email = 'eveline@gena.com';
                $email = new Email();
                $email->email = $ans->email;
                $email->name = $ans->name;
                $email->save();
            $ans->comment = 'Вы правы, будет 6';
            $ans->status = '2';
        $que->answers()->save($ans);

        $que = new Question();
        $que->name = 'Дикобраз Петрович';
        $que->email = 'dikobraz@dikobraz.dikobraz';
            $email = new Email();
            $email->email = $que->email;
            $email->name = $que->name;
            $email->save();
        $que->comment =  'Сколько игл у дикобраза???';
        $que->status = '2';
        $que->save();
    }
}
