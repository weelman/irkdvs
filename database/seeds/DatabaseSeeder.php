<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('answers')->delete();
        DB::table('questions')->delete();
        DB::table('emails')->delete();

        $this->call(QueansSeeder::class);
    }
}
