<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Question extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'name', 'email', 'comment', 'status', 'created_at'
    ];

    public function answers()
	{
        return $this->hasMany('App\Answer');
    }
    public function dates()
    {
        return $this->created_at->format('j.m.Y h:i:s');
    }
    public function getColor()
    {
        switch ($this->status) {
            case 0:
                return "danger";
                break;
            case 1:
                return "success";
                break;
            case 2:
                return "warning";
                break;
        }
    }
}
