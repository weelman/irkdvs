<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Blog extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title', 'description', 'pic', 'created_at'
    ];

    public function elements()
    {
        return $this->hasMany('App\Element');
    }
}
