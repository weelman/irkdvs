<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'number', 'url', 'type'
    ];

    public function work()
    {
        return $this->belongsTo('App\Work');
    }

    public function realization()
    {
        return $this->belongsTo('App\Realization');
    }
}
