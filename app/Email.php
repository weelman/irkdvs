<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'name', 'email', 'created_at'
    ];


    public function dates()
    {
        return $this->created_at->format('j.m.Y h:i:s');
    }
}
