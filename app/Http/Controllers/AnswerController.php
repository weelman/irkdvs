<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;
use App\Email;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function answerModer()
    {
        if(Answer::where('status', '2')->count()>0) {
            $answer_moder = Answer::where('status', '2')->orderBy('created_at', 'desc')->paginate(15);
            $checkbox = 'moder';
        } else {
            $answer_moder = Answer::orderBy('created_at', 'desc')->paginate(15);
            $checkbox = 'all';
        }
        return view('back.answer_moder')->with([
            'answer_moder'=>$answer_moder,
            'checkbox'=>$checkbox
        ]);
    }
    public function answerModerFinder(Request $request)
    {
        if($request->checkbox == 'all')
            $answer_moder = Answer::orderBy('created_at', 'desc')->paginate(15);
        elseif ($request->checkbox == 'success')
            $answer_moder = Answer::where('status', '1')->orderBy('created_at', 'desc')->paginate(15);
        elseif ($request->checkbox == 'moder')
            $answer_moder = Answer::where('status', '2')->orderBy('created_at', 'desc')->paginate(15);
        else
            $answer_moder = Answer::where('status', '0')->orderBy('created_at', 'desc')->paginate(15);
        return view('back.answer_moder')->with([
            'answer_moder'=>$answer_moder,
            'checkbox'=>$request->checkbox
        ]);
    }

    public function answerPublAjax($id)
    {
        $ques = Answer::findOrFail($id);
        $ques->status = '1';
        $ques->save();
    }
    public function answerDeleteAjax($id)
    {
        $ques = Answer::findOrFail($id);
        $ques->delete();
    }
}
