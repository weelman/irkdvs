<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Element;
use Carbon\Carbon;
use File;

class BlogController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $blog_items = Blog::orderBy('created_at', 'desc')->paginate(15);
        return view('back.blog')->with('blog_items', $blog_items);
    }
    public function add() {
        return view('back.blog_add');
    }
    public function addPost(Request $request) {
//        dd($elems);
//        dd($request->all());
        $blog = new Blog;
        $blog->title = $request->title;
        $blog->description = nl2br($request->description, true);
        if($request->hasFile('pic')) {
            $file = $request->pic;
            $filename = str_random(6) .'.' . $file->getClientOriginalExtension() ?: 'png';
            $file->move(public_path() . '/img/odmen/blog/', $filename);
            $blog->pic = '/img/odmen/blog/' . $filename;
        }
        $blog->save();
        if($request->elem) {
            $elems = $request->elem;
            for ($i = 0; $i < count($elems); $i++) {

                if (is_object($elems[$i])) {
                    $item = new Element();
                    $item->type = '0';
                    $filename = str_random(6) . '.' . $elems[$i]->getClientOriginalExtension() ?: 'png';
                    $elems[$i]->move(public_path() . '/img/odmen/blog/', $filename);
                    $item->content = '/img/odmen/blog/' . $filename;
                    $item->blog()->associate($blog);
                    $item->save();
                } else {
                    if (!empty($elems[$i])) {
                        $item = new Element();
                        $item->type = '1';
                        $item->content = nl2br($elems[$i], true);
                        $item->blog()->associate($blog);
                        $item->save();
                    }
                }
            }
        }

        \Session::flash('ses', 'Статья успешно добавлена!');
        return redirect('/admin/blog/');
    }

    public function edit($id) {
        $blog = Blog::findOrFail($id);
        return view('back.blog_edit')->with('blog', $blog);
    }

    public function editPost(Request $request, $id) {
        $blog = Blog::findOrFail($id);
//        dd($blog->elements);
        $count_elem_db = $blog->elements->count();
        if($request->elem)
            $count_elem_req = count($request->elem);
        else
            $count_elem_req = 0;
        $blog->title = $request->title;
        $blog->created_at = $request->created_at;
        $blog->description = nl2br($request->description, true);
        $elements = $blog->elements;
        $reqelem = $request->elem;
        if($request->hasFile('pic')) {
            $file = $request->pic;
            $filename = str_random(6) .'.' . $file->getClientOriginalExtension() ?: 'png';
            $file->move(public_path() . '/img/odmen/blog/', $filename);
            $blog->pic = '/img/odmen/blog/' . $filename;
        }

//        пробегаемся по базе, удаляем лишние файлы
            foreach ($elements as $item) {
    //            перебираем базу с файлами
                if ($item->type==0) {
                    $link = $item->content;
                    $savedd = false;
                    foreach ($request->elem as $req_item) {
    //                    перебераем реквест
                        if ($link == $req_item) {
    //                        если урл найдет
                            $savedd = true;
                        }
                    }
                    if (!$savedd) {
    //                    удаляем, если не нашли такой юрл
                        File::delete(public_path() . $item->content);
                    }
                }
            }
        $blog->save();
        for ($i=0; $i<$count_elem_req; $i++) {
//            dd($elements);
//            проверяем равно значение request значению из базы
            if ($i>=$count_elem_db) {
                $elements[$i] = new Element();
            }
            if ($request->elem[$i]!=$elements[$i]->content) {
//                проверяем тип элемента
                if (is_object($request->elem[$i])) {
                    $elements[$i]->type = '0';
                    $filename = str_random(6) . '.' . $reqelem[$i]->getClientOriginalExtension() ?: 'png';
                    $reqelem[$i]->move(public_path() . '/img/odmen/blog/', $filename);
                    $elements[$i]->content = '/img/odmen/blog/' . $filename;
                    $elements[$i]->blog()->associate($blog);
                    $elements[$i]->save();
                } else {
                    $elements[$i]->content = nl2br($reqelem[$i], true);
                    if (stripos($reqelem[$i], '/img/')) {
//                        dd($reqelem[$i]);
                        $elements[$i]->type = '0';
                    } else {
//                        dd($elements[$i]);
                        $elements[$i]->type = '1';
                    }
                    if ($i>=$count_elem_db) {
                        $elements[$i]->blog()->associate($blog);
                    }
                    $elements[$i]->save();
                }
            }
        }
        if ($count_elem_db>$count_elem_req) {
            for ($i=$count_elem_req; $i<$count_elem_db; $i++) {
                $elements[$i]->delete();
            }
        }

        \Session::flash('ses', 'Изменения произведены успешно!');
        return redirect('/admin/blog/');
    }

//    public function editPostOld(Request $request, $id) {
//        dd($request->all());
//        $blog = Blog::findOrFail($id);
//        if ($blog->elements) {
//            foreach ($blog->elements as $elem) {
//                if($elem->type==0) {
//                    File::delete(public_path() . $elem->content);
//                }
//                $elem->delete();
//            }
//        }
//
//        $blog->title = $request->title;
//        $blog->description = $request->description;
//        if($request->hasFile('pic')) {
//            $file = $request->pic;
//            $file->move(public_path() . $blog->pic);
//        }
//        $blog->save();
//        if($request->elem) {
//            $elems = $request->elem;
//            for ($i = 0; $i < count($elems); $i++) {
//
//                if (is_object($elems[$i])) {
//                    $item = new Element();
//                    $item->type = '0';
//                    $filename = str_random(6) . '.' . $elems[$i]->getClientOriginalExtension() ?: 'png';
//                    $elems[$i]->move(public_path() . '/img/odmen/blog/', $filename);
//                    $item->content = '/img/odmen/blog/' . $filename;
//                    $item->blog()->associate($blog);
//                    $item->save();
//                } else {
//                    if (!stripos($elems[$i], '/img/')) {
//                        $item = new Element();
//                        $item->type = '1';
//                        $item->content = $elems[$i];
//                        $item->blog()->associate($blog);
//                        $item->save();
//                    } else {
//                        $item = new Element();
//                        $item->type = '1';
//                        $item->content = $elems[$i];
//                        $item->blog()->associate($blog);
//                        $item->save();
//                    }
//                }
//            }
//        }
//        return redirect('/admin/blog/');
//    }




    public function deleteAjax($id) {
        $blog = Blog::findOrFail($id);
        File::delete(public_path() . $blog->pic);
        if ($blog->elements) {
            foreach ($blog->elements as $elem) {
                if($elem->type==0) {
                    File::delete(public_path() . $elem->content);
                }
                $elem->delete();
            }
        }
        $blog->delete();
    }
}
