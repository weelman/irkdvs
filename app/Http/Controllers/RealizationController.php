<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Realization;
use App\Photo;
use File;

class RealizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $realizations = Realization::orderBy('created_at', 'desc')->paginate(15);
        return view('back.realization')->with('realizations', $realizations);
    }

    public function add()
    {
        return view('back.realization_add');
    }

    public function addPost(Request $request)
    {
        $files = $request->pic;
        $realization = new Realization();
        $realization->title = $request->title;
        $realization->description = $request->description;
        $realization->garant = $request->garant;
        $realization->price = $request->price;
        $realization->status = $request->status;
        $realization->save();
        if($request->hasFile('pic'))
        {
            foreach ($files as $file) {
//                dd($request->file('pic'));
                $picture = new Photo;
                $filename = str_random(4) .'.' . $file->getClientOriginalExtension() ?: 'png';
                $file->move(public_path() . '/img/odmen/realizations/', $filename);
                $picture->url = '/img/odmen/realizations/' . $filename;
                $picture->type = '0';
                $picture->realization()->associate($realization);
                $picture->save();
            }
        }
        \Session::flash('ses', 'Деталь успешно добавлена!');
        return redirect('/admin/realizations');
    }

    public function edit($id)
    {
        $realization = Realization::findOrFail($id);
        return view('back.realization_edit')->with('realization', $realization);
    }
    public function editPost(Request $request, $id)
    {
//        dd($request->all());
        $realization = Realization::findOrFail($id);
        $realization->title = $request->title;
        $realization->description = $request->description;
        $realization->garant = $request->garant;
        $realization->price = $request->price;
        $realization->status = $request->status;
        $realization->save();
        if($request->hasFile('pic')&&(count($request->pic)<=(6-count($realization->photos))))
        {
            foreach($request->pic as $file)
            {
                $picture = new Photo;
                $filename = str_random(6) .'.' . $file->getClientOriginalExtension() ?: 'png';
                $file->move(public_path() . '/img/odmen/realizations/', $filename);
                $picture->url = '/img/odmen/realizations/' . $filename;
                $picture->type = '0';
                $picture->realization()->associate($realization);
                $picture->save();
            }
//            dd($request->pic);

        }

        \Session::flash('ses', 'Измения произведены успешно!');
//        dd(count($realization->photos));
        return redirect('/admin/realizations');
    }
    public function changePhotoPost(Request $request, $id)
    {
        $file = $request->photo;
        $photo = Photo::findOrFail($request->number);
        $file->move(public_path() . '/img/odmen/realizations/', $photo->url);

        \Session::flash('ses', 'Фото успешно изменено! Нажмите "ctrl+shift+r" если фото не обновилось');
        return redirect($request->server('HTTP_REFERER'));

    }

    public function deleteAjax($id)
    {
        $realization = Realization::findOrFail($id);
        $photos = $realization->photos;
        foreach($photos as $photo)
        {
//            dd($photo->url);
            File::delete(public_path() . $photo->url);
            $photo->delete();
        }
        $realization->delete();
    }
}
