<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Answer;
use App\Question;
use App\Email;
use App\Work;
use App\Realization;
use App\Blog;
use \Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;

class FrontController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->get();
        return view('front.index')->with('blogs', $blogs);
    }
    public function gallery()
    {
        $works = Work::orderBy('created_at', 'desc')->paginate(10);
        return view('front.gallery')->with('works', $works);
    }
    public function answer()
    {
        $questions = Question::where('status', '1')->orderBy('created_at', 'desc')->paginate(25);
        return view('front.answer-question')->with('questions', $questions);
    }
    public function postAddAnswer(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:22',
            'email' => 'required|string|max:32|email',
            'captcha' => 'required|captcha'
        ]);
        if ($request->question)
            $qa = new Answer;
        else
            $qa = new Question;
        $qa->name = $request->name;
        $qa->email = $request->email;
        $qa->comment = $request->comment;
        if ($request->question)
            $qa->question()->associate($request->question);
        $qa->status = '2';
        $qa->save();
        $email = Email::firstOrCreate(['email'=>$request->email], ['email'=>$request->email, 'name'=>$request->name]);

    }


    public function refreshCaptcha()
    {
        return response()->json(['captcha'=>captcha_img()]);
    }

    public function realization()
    {
        $realizations = Realization::orderBy('created_at', 'desc')->paginate(10);
        return view('front.works.realizaciya')->with('realizations', $realizations);
    }

    public function blog()
    {

        $blog_items = Blog::orderBy('created_at', 'desc')->paginate(10);
        return view('front.blog')->with('blog_items', $blog_items);
    }
    public function blogitem($id)
    {
        $item = Blog::findOrFail($id);
        $prev = Blog::where('id', '<', $item->id)->max('id');
        $next = Blog::where('id', '>', $item->id)->min('id');
        return view('front.bitem')->with(['item'=>$item, 'prev'=>$prev, 'next'=>$next]);
    }

}
