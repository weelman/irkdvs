<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $emails = Email::paginate(25);
        return view('back.email')->with('emails', $emails);
    }
    public function deleteAjax($id)
    {
        $email = Email::findOrFail($id);
        $email->delete();
    }
}
