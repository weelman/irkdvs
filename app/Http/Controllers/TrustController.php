<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class TrustController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        return view('back.trust');
    }
    public function editPost(Request $request)
    {
        if(!$request->go) {
            \Session::flash('ses', 'Вы не загрузили фото на замену');
            return redirect('/admin/trust');
        }
        foreach ($request->go as $key=>$value) {
//            dd($key);
            File::delete(public_path() . '/img/trust/trust_' . $key . '.jpg');
            $filename = 'trust_' . $key . '.jpg';
            $value->move(public_path() . '/img/trust/', $filename);
        }
        \Session::flash('ses', 'Фото изменены! Если вы не увидели изменения, то скорей всего старые изображения закэшировались. Для их обновления нажмите ctrl+F5 или shift+F5');
        return redirect('/admin/trust');
    }

}
