<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Work;
use App\Photo;
use File;

class WorkController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $works = Work::paginate(15);
        return view('back.work')->with('works', $works);
    }

    public function add()
    {
        return view('back.work_add');
    }

    public function addPost(Request $request)
    {
        $files = $request->pic;
        $work = new Work;
        $work->title = $request->title;
        $work->description = $request->description;
        $work->save();
        if($request->hasFile('pic'))
        {
            foreach ($files as $file) {
//                dd($request->file('pic'));
                $picture = new Photo;
                $filename = str_random(6) .'.' . $file->getClientOriginalExtension() ?: 'png';
                $file->move(public_path() . '/img/odmen/works/', $filename);
                $picture->url = '/img/odmen/works/' . $filename;
                $picture->type = '0';
                $picture->work()->associate($work);
                $picture->save();
            }
        }
        \Session::flash('ses', 'Работа успешно добавлена!');
        return redirect('/admin/works');
    }

    public function edit($id)
    {
        $work = Work::findOrFail($id);
        return view('back.work_edit')->with('work', $work);
    }
    public function deleteAjax($id)
    {
        $work = Work::findOrFail($id);
        $photos = $work->photos;
        foreach($photos as $photo)
        {
//            dd($photo->url);
            File::delete(public_path() . $photo->url);
            $photo->delete();
        }
        $work->delete();
    }
    public function editPost(Request $request, $id)
    {
//        dd($request->all());
        $work = Work::findOrFail($id);
        $work->title = $request->title;
        $work->description = $request->description;
        $work->save();
        if($request->hasFile('pic')&&(count($request->pic)<=(6-count($work->photos))))
        {
            foreach($request->pic as $file)
            {
                $picture = new Photo;
                $filename = str_random(6) .'.' . $file->getClientOriginalExtension() ?: 'png';
                $file->move(public_path() . '/img/odmen/works/', $filename);
                $picture->url = '/img/odmen/works/' . $filename;
                $picture->type = '0';
                $picture->work()->associate($work);
                $picture->save();
            }
//            dd($request->pic);

        }
        \Session::flash('ses', 'Изменения успешно внесены!');

//        dd(count($work->photos));
        return redirect('/admin/works');
    }
    public function deletePhotoAjax($id)
    {
        $photo = Photo::findOrFail($id);
        File::delete(public_path() . $photo->url);
        $photo->delete();
    }
    public function changePhotoPost(Request $request, $id)
    {
        $file = $request->photo;
        $photo = Photo::findOrFail($request->number);
        $file->move(public_path() . '/img/odmen/works/', $photo->url);
        \Session::flash('ses', 'Фото успешно изменено! Если изменения не произошли, то перезагрузите страницу ctrl+shift+r');
        return redirect($request->server('HTTP_REFERER'));

    }
}
