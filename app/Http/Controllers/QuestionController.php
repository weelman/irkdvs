<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;
use App\Email;


class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.index');
    }
    public function questionModer()
    {
        if(Question::where('status', '2')->count()>0) {
            $question_moder = Question::where('status', '2')->orderBy('created_at', 'desc')->paginate(15);
            $checkbox = 'moder';
        } else {
            $question_moder = Question::orderBy('created_at', 'desc')->paginate(15);
            $checkbox = 'all';
        }
        return view('back.question_moder')->with([
            'question_moder'=>$question_moder,
            'checkbox'=>$checkbox
        ]);
    }
    public function questionModerFinder(Request $request)
    {
        if($request->checkbox == 'all')
            $question_moder = Question::orderBy('created_at', 'desc')->paginate(15);
        elseif ($request->checkbox == 'success')
            $question_moder = Question::where('status', '1')->orderBy('created_at', 'desc')->paginate(15);
        elseif ($request->checkbox == 'moder')
            $question_moder = Question::where('status', '2')->orderBy('created_at', 'desc')->paginate(15);
        else
            $question_moder = Question::where('status', '0')->orderBy('created_at', 'desc')->paginate(15);
        return view('back.question_moder')->with([
            'question_moder'=>$question_moder,
            'checkbox'=>$request->checkbox
        ]);
    }
    public function questionEdit($id)
    {
        $question = Question::findOrFail($id);
        return view('back.question_moder_edit')->with('question', $question);
    }
    public function questionEditPost(Request $request, $id)
    {
//        dd($request);
        $question = Question::findOrFail($id);
        $ans = new Answer;
        $ans->name = $request->name;
        $ans->email = 'admin@irkdvs.ru';
        $ans->comment = $request->comment;
        $ans->status = '1';
        $ans->is_admin = '1';
        $ans->question()->associate($question);
        $ans->save();
        $question->status = '1';
        $question->save();
        \Session::flash('ses', 'Комментарий опубликован, теперь его могут увидеть все пользователи!');
        return redirect('/admin/question/');
    }
    public function questionDelete($id)
    {
        $ques = Question::findOrFail($id);
        foreach($ques->answers as $answ) {
            $answ->delete();
        }
        $ques->delete();
        \Session::flash('ses', 'Комментарий и все ответы удалены!');
        return redirect('/admin/question/');
    }
    public function questionWithout($id)
    {
        $ques = Question::findOrFail($id);
        $ques->status = '1';
        $ques->save();
        \Session::flash('ses', 'Комментарий опубликован без ответа, рекомендуем ответить на неотвеченные вопросы');
        return redirect('/admin/question/');
    }
}
