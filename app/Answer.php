<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;

class Answer extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'question_id', 'name', 'email', 'comment', 'status', 'created_at', 'is_admin'
    ];

    public function question()
	{
        return $this->belongsTo('App\Question');
    }

    public function dates()
    {
        return $this->created_at->format('j.m.Y h:i:s');
    }
    public function img()
    {
        if ($this->is_admin)
            $img = '/img/icons/human_blue.jpg';
        else
            $img = '/img/icons/human_white.jpg';
        return $img;

    }
    public function getColor()
    {
        switch ($this->status) {
            case 0:
                return "danger";
                break;
            case 1:
                return "success";
                break;
            case 2:
                return "warning";
                break;
        }
    }

}
