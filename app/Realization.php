<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Realization extends Model
{
    protected $fillable = [
        'title', 'description', 'garant', 'price', 'status'
    ];

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }
    public function getStatus()
    {
        if($this->status==0)
            return 'В продаже';
        else
            return 'Продано';
    }
}
