<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'blog_id', 'content', 'type', 'created_at'
    ];

    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }
}
