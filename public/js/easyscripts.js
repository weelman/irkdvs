$('ul.navbar-nav div.dropdown').hover(function() {
    if($('body').width()>1000)
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(400);
}, function() {
    if($('body').width()>1000)
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(400);
});
