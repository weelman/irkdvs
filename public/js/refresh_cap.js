var recapcha = (function (){
    $.ajax({
        type: 'GET',
        url: '/refresh_captcha',
        success: function(data){
            $('.captcha span').html(data.captcha);
        }
    })
});
$('.refresh_captcha').click(recapcha);
$('#addcomment a').click(function(event){
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top-$('header').height()-20
        }, 1200, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
});
$('.questionid').click(function(event){
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top-$('header').height()-20
        }, 1200, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
    var queid = $(this).data('questId');
    $('#question').val(queid);
//    $('#addcomment').text('');
    $('#addcomment span').text('Ответить на вопрос, ');
    $('#addcomment a').attr('href', '#q' + $(this).data('questId'));
    $('#addcomment a').text('#' + $(this).data('who'));
});

$("#ajax-form").submit(function(e) {
     e.preventDefault();
    $('#captchaerror').hide();

     var formURL = $(this).attr("action");
     var formmethod = $(this).attr("method");
     var postData = $(this).serialize();

     $.ajax({
         type: formmethod,
         url: formURL,
         data:postData,
         cache: false,

         success: function (jqXHR, textStatus, errorThrown) {
//             console.log('Good!');
             $('#ajax-form')[0].reset();
             $('#addcomment span').text('Добавить комментарий');
             $('#addcomment a').attr('href', '');
             $('#addcomment a').text('');
             alert('Успех! Ваш комментарий появится на сайте после модерации.');
         },

         error: function(jqXHR, textStatus, errorThrown)
         {
//             console.log(jqXHR.responseText);
             recapcha();
             $('#captchaerror').show();
         }

     });

     return false;
 });
