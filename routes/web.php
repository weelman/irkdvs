<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', ['as'=> '/', 'uses'=>'FrontController@index'])->middleware('guest');;
Route::get('/refresh_captcha', 'FrontController@refreshCaptcha');
Route::get('/works-gallery','FrontController@gallery');
Route::get('/blog','FrontController@blog');
Route::get('/blog/{id}','FrontController@blogitem');
Route::get('/contacts', function() {
    return view('front.contacts');
});
Route::get('/confidencial', function() {
    return view('front.confidencial');
});
Route::get('/answer-question', ['as'=> 'answer-question', 'uses'=>'FrontController@answer']);
Route::post('/postAddAnswer', 'FrontController@postAddAnswer');


Route::group(['prefix'=>'works'], function () {
    Route::get('gbc', function () {
        return view('front.works.gbc');
    });
    Route::get('block', function () {
        return view('front.works.block');
    });
    Route::get('collector', function () {
        return view('front.works.collector');
    });
    Route::get('remont_posteli_kolenvala_i_raspredvala', function(){
        return view('front.works.remont_posteli_kolenvala_i_raspredvala');
    });
    Route::get('kardannii-val', function () {
        return view('front.works.kard_val');
    });
    Route::get('kolenchatiy-val', function () {
        return view('front.works.kol_val');
    });
    Route::get('mahovik', function () {
        return view('front.works.mahovik');
    });
    Route::get('realizaciya-detaley', 'FrontController@realization');
    Route::get('shatun', function () {
        return view('front.works.shatun');
    });
    Route::get('tormozniye-i-barabanniye-diski', function () {
        return view('front.works.torm_i_bar_diski');
    });
});

Route::get('/admin', 'QuestionController@index');

//questions
Route::get('/admin/question/', 'QuestionController@questionModer');
Route::get('/admin/question/find', 'QuestionController@questionModerFinder');
Route::get('/admin/question/{id}', 'QuestionController@questionEdit');
Route::post('/admin/question/Post/{id}', 'QuestionController@questionEditPost');
Route::get('/admin/question/delete/{id}', 'QuestionController@questionDelete');
Route::get('/admin/question/nocomment/{id}', 'QuestionController@questionWithout');

//answers
Route::get('/admin/answer/', 'AnswerController@answerModer');
Route::get('/admin/answer/find', 'AnswerController@answerModerFinder');
Route::get('/admin/answer/publAjax/{id}', 'AnswerController@answerPublAjax');
Route::get('/admin/answer/deleteAjax/{id}', 'AnswerController@answerDeleteAjax');

//email
Route::get('/admin/email/', 'EmailController@index');
Route::get('/admin/email/deleteAjax/{id}', 'EmailController@deleteAjax');

//Наши работы
Route::get('/admin/works/', 'WorkController@index');
Route::get('/admin/works/add', 'WorkController@add');
Route::post('/admin/works/addPost', 'WorkController@addPost');
Route::get('/admin/works/edit/{id}', 'WorkController@edit');
Route::get('/admin/works/deleteAjax/{id}', 'WorkController@deleteAjax');
Route::post('/admin/works/editPost/{id}', 'WorkController@editPost');
Route::post('/admin/works/changePhotoPost/{id}', 'WorkController@changePhotoPost');
Route::get('/admin/works/deletePhoto/{id}', 'WorkController@deletePhotoAjax');

//Реализация деталей
Route::get('/admin/realizations/', 'RealizationController@index');
Route::get('/admin/realizations/add', 'RealizationController@add');
Route::post('/admin/realizations/addPost', 'RealizationController@addPost');
Route::get('/admin/realizations/edit/{id}', 'RealizationController@edit');
Route::post('/admin/realizations/editPost/{id}', 'RealizationController@editPost');
Route::post('/admin/realizations/changePhotoPost/{id}', 'RealizationController@changePhotoPost');
Route::get('/admin/realizations/deletePhoto/{id}', 'WorkController@deletePhotoAjax');
Route::get('/admin/realizations/deleteAjax/{id}', 'RealizationController@deleteAjax');

//Блог
Route::get('/admin/blog/', 'BlogController@index');
Route::get('/admin/blog/add', 'BlogController@add');
Route::post('/admin/blog/addPost', 'BlogController@addPost');
Route::get('/admin/blog/edit/{id}', 'BlogController@edit');
Route::post('/admin/blog/editPost/{id}', 'BlogController@editPost');
Route::get('/admin/blog/deleteAjax/{id}', 'BlogController@deleteAjax');

//Доверяйте нам
Route::get('/admin/trust/', 'TrustController@edit');
Route::post('/admin/trust/post', 'TrustController@editPost');
