<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/favicon.ico">


  <title>@if(isset($title)) {{ $title }} @endif</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link href="/odmenka/authpage.css" rel="stylesheet">
@yield('css')
  <!-- Custom styles for this template -->


  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


</head>

<body>
{!! Html::macro('activeState', function($url){
   return (request()->is($url)) ? 'active' : '';
});!!}


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand brand_image" href="/"> <img class="logo" src="/img/logos/logo_blue.png" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ml-auto">
            <li class="nav-item {{HTML::activeState('admin')}}"> <a class="nav-link" href="/admin">Главная <span class="sr-only">(current)</span></a> </li>
            <li class="nav-item {{HTML::activeState('admin/question*')}}"> <a class="nav-link" href="/admin/question/">Комментарии</a> </li>
            <li class="nav-item {{HTML::activeState('admin/answer*')}}"> <a class="nav-link" href="/admin/answer/">Ответы</a> </li>
            <li class="nav-item {{HTML::activeState('admin/email*')}}"> <a class="nav-link" href="/admin/email/">База email</a> </li>
            <li class="nav-item {{HTML::activeState('admin/works*')}}"> <a class="nav-link" href="/admin/works/">Наши работы</a> </li>
            <li class="nav-item {{HTML::activeState('admin/realizations*')}}"> <a class="nav-link" href="/admin/realizations/">Реализация деталей</a> </li>
            <li class="nav-item {{HTML::activeState('admin/blog*')}}"> <a class="nav-link" href="/admin/blog/">Блог</a> </li>
            <li class="nav-item {{HTML::activeState('admin/trust*')}}"> <a class="nav-link" href="/admin/trust/">Фото вы можете доверять</a> </li>
        </ul>
        <form action="/logout" class="navbar-form navbar-right" method="post"> <span class="navbar-text">{{ Auth::user()->email }} </span> {{ csrf_field() }}
            <button type="submit" class="btn btn-danger ml-3">Выход</button>
        </form>
    </div>
</nav>




    <div class="container-fluid">
        <div class="row">
            <div class="col-2 p-0 text-center">
                <div class="nav flex-column nav-pills">
                  <a class="nav-link {{HTML::activeState('admin')}}" href="/admin">Главная </a>
                  <a class="nav-link {{HTML::activeState('admin/question*')}}" href="/admin/question">Комментарии @if((App\Question::where('status', '2')->count())>0)<span class="badge badge-danger badge-pill badge-question">{{ App\Question::where('status', '2')->count() }}</span>@endif</a>
                  <a class="nav-link {{HTML::activeState('admin/answer*')}}" href="/admin/answer">Ответы @if((App\Answer::where('status', '2')->count())>0)<span class="badge badge-danger badge-pill badge-answer">{{ App\Answer::where('status', '2')->count() }}</span>@endif</a>
                  <a class="nav-link {{HTML::activeState('admin/email*')}}" href="/admin/email">База email <span class="badge badge-success badge-pill badge-email">{{ App\Email::all()->count() }}</span></a>
                  <a class="nav-link {{HTML::activeState('admin/works*')}}" href="/admin/works">Наши работы</a>
                  <a class="nav-link {{HTML::activeState('admin/realizations*')}}" href="/admin/realizations">Реализация деталей</a>
                  <a class="nav-link {{HTML::activeState('admin/blog*')}}" href="/admin/blog">Блог</a>
                  <a class="nav-link {{HTML::activeState('admin/trust*')}}" href="/admin/trust">Фото вы можете доверять</a>
                </div>
            </div>

        <main class="col-10 main">
            @yield('backcontent')
        </main>
        </div>
    </div>

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    @yield('javascript')
</body>
</html>
