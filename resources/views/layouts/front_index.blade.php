<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="Разработчик Litvinenko Digital"> @yield('meta')
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/front.css">
      @yield('css') </head>

<body>
   <div class="back_form__send">
        <div class="back_form__send-in">
            <div class="back_form__send-new">
                <p>
                    <big>Спасибо!</big>
                    <br>Ваша заявка принята.
                    <br>Мы свяжемся с Вами в ближайшее время.
                </p>
            </div>
        </div>
   </div>
   <div class="back_form">
       <div class="back_first">
            <svg viewport="0 0 1 1" version="1.1" xmlns="http://www.w3.org/2000/svg" id="close-form">
                <line x1="1" y1="44" x2="44" y2="1" stroke="white" stroke-width="3"></line>
                <line x1="1" y1="1" x2="44" y2="44" stroke="white" stroke-width="3"></line>
            </svg>
            <div class="back_second">
                <p>Вы можете заказать
                <br>доставку деталей в Иркутске,
                <br>а также воспользоваться услугами
                <br>транспортных компаний - оставьте заявку!</p>
                <form action="" id="form">
                    <div class="d-block">
                        <input type="text" name="name" placeholder="ИМЯ" required>
                        <input type="text" name="item" placeholder="ДЕТАЛЬ" required>
                    </div>
                    <div class="d-block">
                        <input type="number" name="phone" placeholder="ТЕЛЕФОН" required>
                        <input type="email" name="email" placeholder="ПОЧТА" required>
                    </div>
                    <br>
                    <div class="d-block">
                        <div class="d-block">
                            <textarea name="comment" rows="4" placeholder="КОММЕНТАРИЙ"></textarea>
                        </div>
                        <button>ОТПРАВИТЬ</button>
                        <span>я соглашаюсь с политикой конфиденциальности</span>
                    </div>
                </form>
            </div>
       </div>
   </div>
    <div class="wrapper">
       @yield('afterhead')
        <header class="header">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-expand-lg navbar-dark">
                        <div class="col-lg-1 col-3 header__logo p-0">
                            <a href="/"><img src="/img/logos/logo_blue.png" alt="Логотип irkdvs"></a>
                        </div>
                        <a href="tel:83952671514" class="d-lg-none d-block numbmob">8(3952)-67-15-14</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav m-auto">
                                <div class="nav-item dropdown">
                                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Перечень наших работ
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="/works/gbc">Ремонт ГБЦ</a>
                                        <a class="dropdown-item" href="/works/block">Блок</a>
                                        <a class="dropdown-item" href="/works/shatun">Ремонт шатуна</a>
                                        <a class="dropdown-item" href="/works/remont_posteli_kolenvala_i_raspredvala">Ремонт постели коленвала и распредвала</a>
                                        <a class="dropdown-item" href="/works/kardannii-val">Карданный вал</a>
                                        <a class="dropdown-item" href="/works/collector">Коллектор</a>
                                        <a class="dropdown-item" href="/works/mahovik">Маховик</a>
                                        <a class="dropdown-item" href="/works/kolenchatiy-val">Коленчатый вал</a>
                                        <a class="dropdown-item" href="/works/tormozniye-i-barabanniye-diski">Тормозные барабаны и диски</a>
                                        <a class="dropdown-item" href="/works/realizaciya-detaley">Реализация деталей</a>
                                    </div>
                                </div>
                                <li class="nav-item"> <a class="nav-link" href="/works-gallery">Наши работы (фото)</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="/blog">Авторский блог</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="/answer-question">Вопрос-Ответ</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="/contacts">Контакты</a> </li>
                            </ul>
                            <button id="show-form" class="form-inline mx-auto mr-lg-0 ml-lg-auto my-2 my-lg-0">ЗАКАЗАТЬ ДОСТАВКУ ДЕТАЛЕЙ</button>
                        </div>
                    </nav>
                </div>
            </div>
        </header> @yield('frontcontent') </div>
    <div class="footer">
        <div class="container">
            <div class="row footer_first m-0">
                <div class="col-lg-2 col-sm-6 col-12 p-0 text-center text-lg-left">
                    <a href="https://www.facebook.com/IRKDVS/?ref=br_rs" target="_blank"><img src="/img/icons/fb.svg" alt=""></a>
                    <a href="https://vk.com/irkdvs38" target="_blank"><img src="/img/icons/vk.svg" alt=""></a>
                    <a href="https://www.instagram.com/irk_dvs/" target="_blank"><img src="/img/icons/instagram.svg" alt=""></a>
                    <a href="https://www.drive2.ru/users/irkdvs/#blog" target="_blank"><img src="/img/icons/drive.svg" alt="drive"></a>
                </div>
                <div class="offset-lg-6 col-lg-4 col-sm-6 col-12 text-lg-left text-center footer_first__links p-0">
                    <a href="/works-gallery" class="d-lg-inline d-none">Наши работы</a>
                    <a href="/blog">Читать блог</a>
                    <a href="/contacts" class="d-lg-inline d-none pr-0">Контакты</a>
                </div>
            </div>
            <div class="row footer_second m-0">
                <div class="col-lg-4 col-sm-6 first p-0 text-left">
                    <address>г. Иркутск,
                    <br>ул. Байкальская 239,  корпус 26
                    <br>(Теплая стоянка Релейного завода)</address>
                    Часы работы :
                    <br>В будни с 9 до 19 часов.
                    <br>В субботу — с 9 до 17 часов.
                </div>
                <div class="col-lg-4 col-sm-6 second">
                    <a href="/works/gbc">Ремонт ГБЦ</a>
                    <a href="/works/block">Блок</a>
                    <a href="/works/shatun">Ремонт шатуна</a>
                    <a href="/works/remont_posteli_kolenvala_i_raspredvala">Ремонт постели коленвала и распредвала</a>
                    <a href="/works/kardannii-val">Карданный вал</a>
                    <a href="/works/collector">Коллектор</a>
                    <a href="/works/mahovik">Маховик</a>
                    <a href="/works/kolenchatiy-val">Коленчатый вал</a>
                    <a href="/works/tormozniye-i-barabanniye-diski">Тормозные и барабанные диски</a>
                    <a href="/works/realizaciya-detaley">Реализация деталей</a>
                </div>
                <div class="col-lg-4">
                    <p>Сайт содержит материалы, охраняемые авторским правом, и средства индивидуализации (логотипы, фирменные знаки). Использование материалов сайта в интернете разрешено только с указанием гиперссылки на сайт www.irkdvs.com. Использование материалов сайта разрешено только с указанием названия сайта «IRKDVS».
                    <br>К нарушителям данного положения применяются все меры, предусмотренные ст. 1301 ГК РФ.</p>
                </div>
            </div>
            <div class="row footer_three align-items-center m-0">
                <div class="col-lg-4 p-0 text-center text-lg-left">
                    <span>© 2008-{{ Carbon\Carbon::today()->year }} irkdvs.com</span>
                </div>
                <div class="col-lg-4 text-center text-lg-left">
                    <a href="/confidencial">Политика конфеденциальности</a>
                </div>
                <div class="col-lg-4 text-center text-lg-left">
                    <a href="https://litvinenko.digital/" target="_blank" style="text-decoration: none"><img src="/img/logo_litvinenko.svg" alt="">Разработка сайта</a>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="/js/easyscripts.js"></script>
    <script>
        var head = 0;
        var heighthead = 0;
        $(document).ready(function(){
            head = $(".header").offset();
            heighthead = $(".header").height();
            $("#close-form").click(function(){
                $(".back_form").animate({
                    opacity: 'toggle',
                    height: 'toggle'
                  }, {
                    duration: 700,
                    specialEasing: {
                      opacity: 'linear',
                      height: 'swing',
                    }
                  });

            });
            $("#show-form").click(function(){
                $(".back_form").animate({
                    opacity: 'toggle',
                    height: 'toggle'
                  }, {
                    duration: 700,
                    specialEasing: {
                      opacity: 'linear',
                      height: 'swing'
                    }
                  });
            });
            $(document).on('submit', '#form', function(e){
                var data_form = $(this).serialize();
//                console.log(data_form);
                e.preventDefault();
                $.ajax({
                    type: "POST", //Метод отправки
                    url: "/sendmail.php", //путь до php фаила отправителя
                    data: data_form,
                    success: function() {
                        $(".back_form").hide();
                        $(".back_form__send").show();
                        setTimeout(function(){$(".back_form__send").animate({
                            opacity: 'toggle',
                            height: 'toggle'
                          }, {
                            duration: 700,
                            specialEasing: {
                              opacity: 'linear',
                              height: 'swing'
                            }
                          });}, 1000);
                        $('#form').trigger('reset');
                        return false;
                        },
                    error: function() {
                        $(".back_form").hide();
                        $(".back_form__send").show();
                        setTimeout(function(){$(".back_form__send").animate({
                            opacity: 'toggle',
                            height: 'toggle'
                          }, {
                            duration: 700,
                            specialEasing: {
                              opacity: 'linear',
                              height: 'swing'
                            }
                          });}, 1000);
                        $('#form').trigger('reset');
                        return false;
                        },
                    });
            });

        });
        $(window).scroll(function(){
            if ( $(window).scrollTop() >= head["top"] && $(window).scrollTop()!='0' ){
                if(!$(".wrapper").hasClass("wrapper_fixeded")) {
                    $(".wrapper").addClass("wrapper_fixeded");
//                    $("body").css("margin-top", heighthead);
                } else {
                    return false;
                }
            } else {
                if($(".wrapper").hasClass("wrapper_fixeded")){
                    $(".wrapper").removeClass("wrapper_fixeded");
//                    $("body").css("margin-top", 0);
                }
            }
        });
    </script>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter48497339 = new Ya.Metrika({
                    id: 48497339
                    , clickmap: true
                    , trackLinks: true
                    , accurateTrackBounce: true
                    , webvisor: true
                });
            }
            catch (e) {}
        });
        var n = d.getElementsByTagName("script")[0]
            , s = d.createElement("script")
            , f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        }
        else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/48497339" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
      @yield('scripts') </body>

</html>
