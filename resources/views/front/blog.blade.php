@extends('layouts.front') @section('meta')
    <meta name="description" content="">
    <meta name=«title» content="">
    <meta property="og:locale" content="ru_RU">
    <title>Блог</title>
    <link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
    <div class="container blog">
        <div class="row">
            @foreach ($blog_items as $item)
                <div class="col-12">
                    <div class="blog__item">
                        <span>{{ $item->created_at }}</span><br>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-12"><img class="w-100" src="{{ $item->pic }}" alt=""></div>
                            <div class="col-lg-6 col-12"><h2>{{ $item->title }}</h2>
                            <p>{!! str_limit($item->description, 125) !!}</p>
                            <a href="/blog/{{ $item->id }}"><button>Читать далее</button></a></div>
                        </div>
                    </div>
                </div>
            @endforeach
        <div class="mx-auto">
            {{ $blog_items->links() }}
        </div>
        </div>
    </div> @endsection
