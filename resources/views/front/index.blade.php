@extends('layouts.front_index')

@section('meta')
   <meta name="yandex-verification" content="9e4891cabaa37778" />
    <meta name="description" content="Мастерская по ремонту и восстановлению деталей ДВС. Шлифовка ГБЦ, ремонт ГБЦ, ремонт деталей двигателя, гильзование, хонингование, регулировка клапанов">
    <meta name=«title» content="Ремонт ГБЦ Иркутск">
    <link rel="canonical" href="http://irkdvs.com/">
    <meta property="og:locale" content="ru_RU">
    <title>Ремонт ГБЦ Иркутск</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/magnif.css">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 17, // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(52.283202, 104.277974), // New York
                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                }
            ]
            };
            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(52.283202, 104.277974)
                , map: map
                , icon: {
                    url: "/img/logos/mapmarket.png"
                    , scaledSize: new google.maps.Size(72, 72)
                }
            });
        }
    </script>
@endsection

@section('afterhead')

    <div class="video-preview">
       <video autoplay loop id="video-background" muted plays-inline>
          <source src="/img/bg.mp4" type="video/mp4">
        </video>
        <div class="container h-100">
            <div class="row d-lg-flex d-none">
                <div class="offset-lg-1 col-lg-5">
                    <div class="video-preview__div">
                       <img src="/img/icons/address_blue.png" alt="Иконка адреса">
                        <div>
                            <span>Наш адрес</span>
                            <a href="#"><address>г. Иркутск, ул. Байкальская 239, корп. 26</address></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="video-preview__div">
                       <img src="/img/icons/mail_blue.png" alt="иконка почты">
                        <div>
                            <span>Наша почта</span>
                            <a href="mailto:irkdvs@mail.ru">irkdvs@mail.ru</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="video-preview__div">
                       <div>
                           <img src="/img/icons/phone_blue.png" alt="иконка телефона">
                       </div>
                        <div>
                            <span>Наш телефон</span>
                            <a href="tel:83952671514">8(3952)-67-15-14</a>
                            <a href="tel:83952675757">8(3952)-67-57-57</a>
                            <a href="tel:83952601011">8(3952)-60-10-11</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-center video-preview_logo-div">
                <div class="col-lg-auto text-center">
                    <img src="/img/logos/logo_white.png" alt="Логотип irkdvs белый">
                    <h1>мастерская механической обработки<br>
                    деталей двигателя</h1>
                    мы можем то, что нельзя
                    <br>сделать в гараже
                    <button class="go-jack" href="#services">смотреть наши услуги</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('frontcontent')
    <div class="before-head" id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12 before-head__change-image">
                    <div class="col-lg-6 col-4 text-center">
                        <button class="before-head__change-button"></button>
                        <span>НАШ<br>
                        <big>ЦЕХ</big></span>
                    </div>
                    <div class="col-lg-12 col-9 offset-lg-0 offset-3">
                        <div class="row justify-content-end">
                            <img src="/img/before-head/before-head_img_main.png" class="background" alt="">
                            <img src="/img/before-head/before-head_img_1.png" class="before-head__device before-head__device_show" alt="">
                            <img src="/img/before-head/before-head_img_2.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_3.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_4.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_5.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_6.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_7.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_8.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_9.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_10.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_11.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_12.png" class="before-head__device before-head__device_hide" alt="">
                            <img src="/img/before-head/before-head_img_13.png" class="before-head__device before-head__device_hide" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1 before-head__services">
                    <h2>УСЛУГИ:</h2>
                    <p>Ремонт , шлифовка, опрессовка головок блока цилиндров (ГБЦ) — Иркутск.</p>
                    <p>Восстановление постелей коленчатых и распределительных валов в Иркутске.</p>
                    <p>Расточка и хонингование блоков.
                    <br>Изготовление клапанов , седел,
                    <br>направляющих втулок, стаканов форсунок.
                    <br>Ремонт шатунов.
                    <br>Ремонт маховиков.</p>

                    <p>Ремонт карданных валов в Иркутске.
                    <br>Проточка передних тормозных дисков.
                    <br>Расточка задних тормозных барабанов.
                    <br>Регулировка клапанов.</p>
                </div>
            </div>
        </div>
        <div class="container before-head__about">
            <div class="row">
                <div class="col-lg-4 col-12 pb-lg-0 pb-5">
                    <div class="row">
                        <div class="col-auto">
                            <img src="/img/icons/before-head_1.jpg" alt="">
                        </div>
                        <div class="col">
                            <h2>Качество обработки деталей</h2>
                            <p>На данный момент мы имеем более
                            <br>40 единиц различного оборудования
                            <br>для обработки деталей двигателя.
                            <br>90% оборудования — это станки лучших
                            <br>станкостроительных фирм
                            <br>в теме ремонт деталей двигателя</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 pb-lg-0 pb-5">
                    <div class="row">
                        <div class="col-auto">
                            <img src="/img/icons/before-head_2.jpg" alt="">
                        </div>
                        <div class="col">
                            <h2>Короткие сроки</h2>
                            <p>Мелкие работы (тестирование,
                            <br>шлифовка гбц, обработка клапанов,
                            <br>обработка седел,ремонт шатунов,
                            <br>шлифовка блока): не более половины дня.
                            <br>Сложные работы (полный ремонт головки
                            <br>с регулировкой клапанов, горизонтально
                            <br>расточные работы, установка седел
                            <br>увеличенного диаметра , расточка
                            <br>и хонингование блока): 1-2 дня.
                            <br>Нестандартные работы: не более 3-5 дней.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12 pb-lg-0">
                    <div class="row">
                        <div class="col-auto">
                            <img src="/img/icons/before-head_3.jpg" alt="">
                        </div>
                        <div class="col">
                            <h2>Точность</h2>
                            <p>Современное машиностроение
                            <br>исключает факт человеческой
                            <br>ошибки.
                            <br>
                            <br>Мы гарантируем точность
                            <br>до нескольких микрон.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="devices">
        <div class="container">
            <div class="row justify-content-lg-between justify-content-center m-0">
                    <h2>ОБОРУДОВАНИЕ — КАЧЕСТВО</h2>
                    <p>Наш выбор компаний производителей станков для обработки деталей двигателей вырабатывался годами, при этом мы отслеживаем и тестируем новинки рынка. Регулярное обновление програмного обеспечения, позволяет нам высокоточно выполнять даже самые не простые задачи.</p>
                    <img src="/img/logos/rottler.jpg" alt="">
                    <img src="/img/logos/amc.jpg" alt="">
                    <img src="/img/logos/sunnen.jpg" alt="">
                    <img src="/img/logos/newen.jpg" alt="">
                    <img src="/img/logos/trego.jpg" alt="">
                    <img src="/img/logos/pmd.jpg" alt="">
                    <img src="/img/logos/comec.jpg" alt="">
                    <img src="/img/logos/berco.jpg" alt="">
                    <img src="/img/logos/serdi.jpg" alt="">
                    <img src="/img/logos/carmec.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="trust">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <h3>ВЫ МОЖЕТЕ НАМ ДОВЕРЯТЬ</h3>
                    <p>Наша мастерская уже 10 лет предоставляет услуги по ремонту,
                    <br>восстановлению и обработке разнообразных деталей двигателя
                    <br>внутреннего сгорания. А именно — головок блока (ГБЦ), блоков
                    <br>цилиндров, шатунов, маховиков, коллекторов любых
                    <br>автомобилей, спецтехники, водно-моторной,
                    <br>сельскохозяйственной и лесозаготовительной техники.</p>
                    <p>Спектр выполняемых нашей мастерской работ включает:
                    <br>тестирование головок блока цилиндров на герметичность,
                    <br>шлифовку и фрезеровку головок блока цилиндров, замену
                    <br>направляющих втулок, изготовление и замену сёдел клапанов,
                    <br>регулировку клапанов, расточку и хонингование блоков
                    <br>цилиндров, обработку плоскости блоков цилиндров, ремонт
                    <br>шатунов, шлифовку маховиков, горизонтально-расточные
                    <br>работы по постели распредвала и коленвала, и еще многое другое.</p>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-6 d-flex" href="/img/trust/trust_1.jpg">
                            <a href="#" class="trusta"><img class="trustimg" href="/img/trust/trust_1.jpg" src="/img/trust/trust_1.jpg" alt=""></a>
                        </div>
                        <div class="col-6 d-flex" href="/img/trust/trust_2.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_2.jpg" alt=""></a>
                        </div>
                        <div class="col-6 d-flex" href="/img/trust/trust_3.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_3.jpg" alt=""></a>
                        </div>
                        <div class="col-6 d-flex" href="/img/trust/trust_4.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_4.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-3 col-6 d-flex" href="/img/trust/trust_5.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_5.jpg" alt=""></a>
                        </div>
                        <div class="col-lg-3 col-6 d-flex" href="/img/trust/trust_6.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_6.jpg" alt=""></a>
                        </div>
                        <div class="col-lg-3 col-6 d-flex" href="/img/trust/trust_7.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_7.jpg" alt=""></a>
                        </div>
                        <div class="col-lg-3 col-6 d-flex" href="/img/trust/trust_8.jpg">
                            <a href="#" class="trusta"><img class="trustimg" src="/img/trust/trust_8.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="delivery">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <h2>ДОСТАВКА</h2>
                    <p>Вы можете заказать доставку деталей в г.Иркутске,
                    <br>а также воспользоваться услугами транспортных
                    <br>компаний, если вы находитесь в другом городе,
                    <br>для доставки на ремонт ваших деталей к нам.
                    <br>
                    <br>Звоните нам по телефону: 8(3952)-60-10-11  </p>
                    <img src="/img/delivery/car.png" alt="">
                </div>
                <div class="col-lg-6 delivery__callback">
                    <big>ЕСТЬ ВОПРОС?</big>
                    <div class="delivery__callback_in">
                        ОСТАВЬТЕ ЗАЯВКУ,
                        <br>МЫ ОТВЕТИМ ВАМ В ТЕЧЕНИИ 24 ЧАСОВ
                        <form action="" class="form_first">
                            <textarea name="message" id="" placeholder="Ваше сообщение..." required></textarea>
                            <div class="row">
                                <div class="col-6">
                                    <input type="text" placeholder="Ваше имя" name="name" required>
                                    <input type="text" placeholder="Ваш E-mail" name="email" required>
                                </div>
                                <div class="col-6">
                                    <input type="text" placeholder="Ваш телефон" name="phone" required>
                                    <input type="text" placeholder="Компания" name="company">
                                </div>
                            </div>
                            <button type="submit">ОТПРАВИТЬ</button>
                            <p>Я согласен на обработку моих персональных данных</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog d-lg-flex d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>НАШ АВТОРСКИЙ БЛОГ</h2>
                    <p>Мы публикуем авторские статьи о ремонте и об интересных проектах на нашем сайте
                    <br>irkdvs.com, в блоге на сайте drive2.ru на нашем канале youtube и в профиле instagram.
                    <br>Заходите, подписывайтесь, задавайте вопросы.</p>
                    <span class="d-block mb-3">ПОСЛЕДНИЕ СТАТЬИ:</span>
                    <div class="row">
                       @foreach($blogs->slice(0, 3) as $item)
                        <div class="col-lg-3 blog_item">
                            <a href="/blog/{{ $item->id }}">
                                <div class="blog_in">
                                    <img class="clock" src="/img/icons/clock_square.jpg" alt="">
                                    <span>{{ $item->created_at }}</span>
                                    <img class="item" src="{{$item->pic}}" alt="">
                                    <p>{{ str_limit($item->description, 100) }}</p>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        <div class="col-lg-3 blog_item soc_network">
                            <p>ПОДПИШИСЬ НА НАС:</p>
                            <div class="blog_in">
                                <div class="row justify-content-between m-0">
                                    <a href="https://www.drive2.ru/users/irkdvs/#blog" target="_blank">
                                        <img src="/img/icons/drive_black.jpg" alt="">
                                        <span>DRIVE 2</span>
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCWP8DBxh3nESQ1lIaiLr5KQ" target="_blank">
                                        <img src="/img/icons/youtube_black.jpg" alt="">
                                        <span>YOUTUBE</span>
                                    </a>
                                    <a href="https://www.instagram.com/irk_dvs/" target="_blank">
                                        <img src="/img/icons/instagram_black.jpg" alt="">
                                        <span>INSTAGRAM</span>
                                    </a>
                                </div>
                                <strong>Внимание!
                                <br>Посмотреть наши работы
                                <br><a href="https://forums.drom.ru/irkutsk/t1152101117.html" target="_blank">можно в нашей теме на DROM.ru,</a>
                                <br>а задать интересующие Вас
                                <br>вопросы - <a href="https://baza.drom.ru/irkutsk/service/repair/remont-golovok-bloka-gbc-rastochka-i-honingovanie-bloka-stanki-s-chpu-27730086.html?action=view&keywords=remont-golovok-bloka-rastochka-i-honingovanie-blokov-cilindrov-remont&id=27730086&sessionGeoId=340" target="_blank">в нашем объявлении
                                <br>на DROM.ru</a> Добро пожаловать!
                                <br>Ждем вопросов и комментариев!</strong>
                            </div>
                        </div>
                    </div>
                    <a class="blog_go" href="/blog" target="_blank">ЧИТАТЬ БЛОГ</a>
                </div>
            </div>
        </div>
    </div>
    <div class="clients">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>НАШИ КЛИЕНТЫ</h2>
                    <p class="d-lg-block d-none">За время существования IRKDVS зарекомендовал себя как надежный подрядчик
                    <br>обслуживающий крупные предприятия и официальных представителей,
                    <br>так и частных клиентов с единичными обращениями.</p>
                    <p class="d-lg-none d-block">За время существования IRKDVS зарекомендовал
                    <br>себя как надежный подрядчик обслуживающий
                    <br>крупные предприятия и официальных представителей,
                    <br>так и частных клиентов с единичными обращениями.</p>
                    <div class="row">
                        <div class="col-lg-2 col-4"><img src="/img/clients/1.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/2.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/3.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/4.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/5.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/6.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/7.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/8.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/9.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/10.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/11.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/12.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/13.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/14.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/15.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/16.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/17.jpg" alt=""></div>
                        <div class="col-lg-2 col-4"><img src="/img/clients/18.jpg" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="certificates">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>НАШИ СЕРТИФИКАТЫ</h2>
                    <p class="d-lg-block d-none">Обмен опытом с производителями деталей двигателя, стал традицией нашей компании.
                    <br>Регулярно посещаем выставки и повышаем собственнную квалификацию, хотя казалось бы куда?
                    <br>А учиться всегда необходимо. Появляются новые станки, новые технологии. Мы всегда на чеку!</p>
                    <p class="d-lg-none d-block">Обмен опытом с производителями деталей двигателя,<br> стал традицией нашей компании. Регулярно посещаем
                    <br> выставки и повышаем собственнную квалификацию,
                    <br>хотя казалось бы куда?
                    <br>
                    <br>А учиться всегда необходимо.
                    <br>Появляются новые станки, новые технологии.
                    <br>Мы всегда на чеку!</p>
                </div>
                <div class="col-3 d-flex"><a href="#" class="trusta"><img class="trustimg" src="/img/sertificates/1.jpg" alt=""></a></div>
                <div class="col-3 d-flex"><a href="#" class="trusta"><img class="trustimg" src="/img/sertificates/2.jpg" alt=""></a></div>
                <div class="col-3 d-flex"><a href="#" class="trusta"><img class="trustimg" src="/img/sertificates/3.jpg" alt=""></a></div>
                <div class="col-3 d-flex"><a href="#" class="trusta"><img class="trustimg" src="/img/sertificates/4.jpg" alt=""></a></div>
                <div class="container-fluid certificates_hidden">
                    <div class="row justify-content-center">
                        <div class="col-3 d-flex"><a href="#" class="trusta"><img class="trustimg" src="/img/sertificates/5.jpg" alt=""></a></div>
                    </div>
                </div>
                <a href="" class="show-cert">смотреть ещё</a>
            </div>
        </div>
    </div>
    <div id="map_place">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>МЫ НА КАРТЕ</h2>
                    <address>г. Иркутск, ул. Байкальская 239, корп. 26 <br>
                    (Теплая стоянка Релейного завода)</address>
                </div>
            </div>
        </div>
        <div id="map"></div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
          </div>
          <div class="modal-body">
            <img src="" id="preview" class="w-100">
          </div>
        </div>
      </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(".trusta").on("click", function() {
           $('#preview').attr('src', $(this).find('.trustimg').attr('src'));
           $('#modal').modal('show');
            return false;
        });
    </script>
    <script>
        $(document).ready(function(){
            $(".go-jack").click(function (event) {
                //отменяем стандартную обработку нажатия по ссылке
                event.preventDefault();

                //забираем идентификатор бока с атрибута href
                var id  = $(this).attr('href'),

                    //узнаем высоту от начала страницы до блока на который ссылается якорь
                    top = $(id).offset().top;

                //анимируем переход на расстояние - top за 1500 мс
                $('body,html').animate({scrollTop: top}, 1500);
            });
            $(document).on('submit', '.form_first', function(e){
                var data_form = $(this).serialize();
//                console.log(data_form);
                e.preventDefault();
                $.ajax({
                    type: "POST", //Метод отправки
                    url: "/sendmail_comp.php", //путь до php фаила отправителя
                    data: data_form,
                    success: function() {
                        $(".back_form__send").fadeIn(500);
                        setTimeout(function(){$(".back_form__send").animate({
                            opacity: 'toggle',
                            height: 'toggle'
                          }, {
                            duration: 700,
                            specialEasing: {
                              opacity: 'linear',
                              height: 'swing'
                            }
                          });}, 1000);
                        $('.form_first').trigger('reset');
                        return false;
                        },
                    error: function() {
                        $(".back_form__send").fadeIn(500);
                        setTimeout(function(){$(".back_form__send").animate({
                            opacity: 'toggle',
                            height: 'toggle'
                          }, {
                            duration: 700,
                            specialEasing: {
                              opacity: 'linear',
                              height: 'swing'
                            }
                          });}, 1000);
                        $('.form_first').trigger('reset');
                        return false;
                        },
                    });
            });
            $(".before-head__change-button").on('click', function(){
                var thiselem = $('.before-head__device_show');
                var nextelem = $('.before-head__device_show').next(".before-head__device");
                if(!$(nextelem).length) {
                    var nextelem = $('.before-head__device:first');
                }
                $(thiselem).addClass('before-head__device_hide').removeClass('before-head__device_show');
                $(nextelem).removeClass('before-head__device_hide').addClass('before-head__device_show');
            });
            $(".show-cert").click(function(){
                $(".certificates_hidden").slideToggle();
                return false;
            });
        });
    </script>

@endsection
