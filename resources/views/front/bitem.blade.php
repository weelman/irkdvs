@extends('layouts.front') @section('meta')

    <meta name=«title» content="{{ $item->title }}">
    <meta name="description" content="{{ str_limit($item->desctiption, 180) }}">
    <meta property="og:locale" content="ru_RU">
    <title>Блог</title>
    <link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
    <div class="container blog">
        <div class="row">
<!--
            <div class="col-3">
                <div class="blog__more">
                    См также
                </div>
            </div>
-->
            <div class="col-12">
                <div class="blog__item">
                        <div class="col-12">
                            <span>{{ $item->created_at }}</span><br>
                            <img class="w-75" src="{{ $item->pic }}" alt="">
                            <h2 class="my-3">{{ $item->title }}</h2>
                            <p class="mb-5">{!! $item->description !!}</p>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                @foreach($item->elements as $elem)
                                    @if($elem->type==0)
                                        <div class="col-6">
                                            <img class="w-100 my-2" src="{{ $elem->content }}" alt="">
                                        </div>
                                    @else
                                        <div class="col-12">
                                            <p class="my-2" style="font-size: 1em">{!! $elem->content !!}</p>
                                        </div>
                                    @endif
                                @endforeach</div>
                        </div>
                    </div>
                </div>
            @if($prev)<div class="col-6">
                <a href="/blog/{{ $prev }}" style="color: black"><Предыдущая статья</a>
            </div>@endif
            @if($next)<div class="col-6 text-right">
                <a href="/blog/{{ $next }}" style="color: black">Следующая статья></a>
            </div>@endif
            </div>
        </div> @endsection

