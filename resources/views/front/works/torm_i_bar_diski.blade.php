@extends('layouts.front') @section('meta')
<meta name="description" content="Расточка тормозных барабанов легкового и грузового транспорта. Обработка поверхности тормозных дисков.">
<meta name=«title» content="Работы по проточке тормозных дисков и барабанов">
<meta property="og:locale" content="ru_RU">
<title>Работы по проточке тормозных дисков и барабанов:</title>
<link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
<div class="works block">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Работы по проточке тормозных дисков и барабанов:</h1>
                <ol>
                    <li>1 Расточка тормозных барабанов легкового и грузового транспорта.</li>
                    <li>2 Обработка поверхности тормозных дисков.</li>
                </ol>
                <p>
                    Мы предлагаем проточку тормозных дисков и расточку тормозных барабанов всех
                    <br>марок легковых и грузовых автомобилей.</p>
                <p>
                    Сроки исполнения: 1-2 дня
                    Следует иметь в виду, что качество торможения автомобиля зависит от площади (пятна)
                    <br>контакта тормозного диска или барабана с колодками. Если прилегание поверхностей
                    <br>плотное и равномерное, тормозной путь автомобиля минимален и качество торможения высокое.
                </p>
                <h2>Проточка тормозных дисков</h2>
                <p>Тормозные диски протачивают для снятия неровностей поверхности, увеличивая
                <br>тем самым пятно контакта диска с тормозным барабаном. В результате проточки дисков:</p>
                <ol>
                    <li>• повышается эффективность торможения;</li>
                    <li>• уменьшается износ тормозных колодок;</li>
                    <li>• прекращаются биение и вибрации;</li>
                    <li>• пропадают нехарактерные звуки: скрип, писк и пр.</li>
                </ol>
                <p>В большинстве случаев проточить диск оказывается достаточно,
                <br>чтобы полностью восстановить работу тормозной системы.</p>
                <h2>Расточка тормозных барабанов</h2>
                <p>Тормозные барабаны растачивают для удаления неровностей поверхности,
                <br>увеличение площади плотного контакта колодок с барабаном, улучшения качества
                <br>торможения и уменьшения износа колодок.</p>
                <ul>
                    <li><strong>Признаками необходимости расточки тормозных барабанов могут быть:</strong></li>
                    <li>• вибрация при торможении;</li>
                    <li>• шум при нажатии тормозной педали;</li>
                    <li>• биение тормозной педали и руля;</li>
                    <li>• ухудшение торможения.</li>
                </ul>
                <p>Про появлении подобных «симптомов» обращайтесь к нашим специалистам.
                <br>Мы проведем диагностику и на ее основании сделаем необходимый ремонт
                <br>тормозной системы вашего автомобиля.</p>
                <div class="row">
                    <div class="col-4">
                        <img src="/img/works/torm/first.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/torm/second.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/torm/third.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/torm/fourth.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/torm/fifth.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/torm/sixth.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> @endsection
