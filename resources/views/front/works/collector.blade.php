@extends('layouts.front') @section('meta')
<meta name="description" content="Восстановление поврежденных резьбовых отверстий. Проверка геометрии и герметичности">
<meta name=«title» content="Коллектор">
<link rel="canonical" href="http://irkdvs.ru/">
<meta property="og:locale" content="ru_RU">
<title>Коллектор</title>
<link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
<div class="works">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Коллектор</h1>
                <ol>
                    <li>• Проверка геометрии и герметичности</li>
                    <li>• Восстановление поврежденных резьбовых отверстий</li>
                    <li>• Удаление заломов</li>
                    <li>• Обработка привалочной плоскости коллектора (фрезерование или шлифовка)</li>
                </ol>
            </div>
        </div>
    </div>
</div> @endsection
