@extends('layouts.front') @section('meta')
<meta name="description" content="Проверка геометрических размеров шатуна. Проверка и подгонка веса шатуна. Замена втулки верхней головки шатуна (ВГШ) (с хонинговальными или расточными работами)">
<meta name=«title» content="Перечень работ производимых над шатуном">
<meta property="og:locale" content="ru_RU">
<title>Перечень работ производимых над шатуном</title>
<link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
<div class="works">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Перечень работ производимых над шатуном:</h1>
                <ol>
                    <br>• Проверка геометрических размеров шатуна.
                    <br>• Проверка и подгонка веса шатуна.
                    <br>• Замена втулки верхней головки шатуна (ВГШ) (с хонинговальными или расточными
                    <br>работами)
                    <br>• Восстановление геометрических размеров нижней головки шатуна (НГШ)
                    <br>(хонингование и расточка НГШ)
                    <br>• Изготовление и замена нестандартных втулок верхней головки шатуна (ВГШ)
                </ol>
                <div class="row">
                    <div class="col-4">
                        <img src="/img/works/shatun/first.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/shatun/second.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/shatun/third.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/shatun/fourth.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/shatun/fifth.jpg" alt="">
                    </div>
                    <div class="col-4">
                        <img src="/img/works/shatun/sixth.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> @endsection
