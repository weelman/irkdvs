@extends('layouts.front') @section('meta')
<meta name="description" content="В этом разделе мы реализуем восстановленные детали двигателя.">
<meta name=«title» content="Продажа восстановленных агрегатов">
<link rel="canonical" href="http://irkdvs.ru/">
<meta property="og:locale" content="ru_RU">
<title>Продажа восстановленных агрегатов</title>
<link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
<div class="realization works">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Продажа восстановленных агрегатов</h1>
                <p>В этом разделе мы реализуем восстановленные детали двигателя.</p>
            </div>
            @foreach($realizations as $realization)
            <div class="col-12">
                <h2>{{ $realization->title }}</h2>
                <p class="status">Статус  «{{ $realization->getStatus() }}»</p>
                <div class="row">
                    @foreach($realization->photos as $photo)
                    <div class="col-lg-3 col-6"><img src="{{ $photo->url }}" alt=""></div>
                    @endforeach
                </div>
                <p class="description">{{ $realization->description }}</p>

                <p class="garant"><strong>Гарантия:</strong> {{ $realization->garant }}</p>

                <p class="price
                ">Стоимость: {{ $realization->price }} рублей</p>
                <br>
            </div>
            @endforeach
            <br><br>
        </div>
    </div>
</div> @endsection
