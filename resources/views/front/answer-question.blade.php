@extends('layouts.front') @section('meta')
<meta name="description" content="">
<meta name=«title» content="">
<meta property="og:locale" content="ru_RU">
<title>Перечень работ производимых над шатуном</title>
<link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
<div class="container works gallery">
    <div class="row">
        <div class="col-12">
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
              <strong>Неверный код с картинки</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <h1>Вопрос-ответ:</h1>
        </div>





        <div class="col-9 asking">
            <p>В комментариях к этой странице Вы можете задать вопрос специалистам мастерской!
            <br>После модерации ваш комментарий обязательно появится на странице.</p>
            <p class="comments">{{ $questions->count() }} комментариев:</p>
           @foreach($questions as $question)
                <div class="question" id="q{!! $question->id !!}">
                    <div class="row">
                        <div class="col-auto">
                            <img src="/img/icons/human_white.jpg" alt="">
                        </div>
                        <div class="col">
                            <p><strong>#{!! $question->id !!} {!! $question->name !!}</strong> <em>спрашивает:</em></p>
                            <span>{!! $question->dates() !!}</span>
                            <p class="text">{!! $question->comment !!}</p>
                            <a href="#captcha" class="questionid" data-who="{!! $question->id !!} {!! $question->name !!}" data-quest-id="{!! $question->id !!}">Ответить</a>
                            @foreach($question->answers->where('status', '1') as $answer)
                            <div class="row answer">
                                <div class="col-auto">
                                    <img src="{{ $answer->img() }}" alt="">
                                </div>
                                <div class="col">
                                    <p><strong>{{ $answer->name }}</strong> <em>отвечает:</em></p>
                                    <span>{{ $answer->dates() }}</span>
                                    <p class="text">{!! $answer->comment !!}</p>
                                    <a href="#captcha" class="questionid" data-who="{!! $question->id !!} {!! $question->name !!}" data-quest-id="{!! $question->id !!}">Ответить</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
            {{ $questions->links() }}
        </div>



        <div class="col-lg-3 col-12">
            <div class="go-blog text-center">
                <img src="img/logos/logo_blue.png" alt="">
                <h3>НАШ АВТОРСКИЙ
                <br>БЛОГ О РЕМОНТЕ</h3>
                <a href="/blog"><button>ЧИТАТЬ БЛОГ</button></a>
            </div>
            <div class="blog_item soc_network">
                <p>ПОДПИШИСЬ НА НАС:</p>
                <div class="blog_in">
                    <div class="row justify-content-between m-0">
                        <a href="https://www.drive2.ru/users/irkdvs/#blog" target="_blank">
                            <img src="/img/icons/drive_black.jpg" alt="">
                            <span>DRIVE 2</span>
                        </a>
                        <a href="https://www.youtube.com/channel/UCWP8DBxh3nESQ1lIaiLr5KQ" target="_blank">
                            <img src="/img/icons/youtube_black.jpg" alt="">
                            <span>YOUTUBE</span>
                        </a>
                        <a href="https://www.instagram.com/irk_dvs/" target="_blank">
                            <img src="/img/icons/instagram_black.jpg" alt="">
                            <span>INSTAGRAM</span>
                        </a>
                    </div>
                    <strong>Внимание!
                    <br>Посмотреть наши работы
                    <br><a href="https://forums.drom.ru/irkutsk/t1152101117.html" target="_blank">можно в нашей теме на DROM.ru,</a>
                    <br>а задать интересующие Вас
                    <br>вопросы - <a href="https://baza.drom.ru/irkutsk/service/repair/remont-golovok-bloka-gbc-rastochka-i-honingovanie-bloka-stanki-s-chpu-27730086.html?action=view&keywords=remont-golovok-bloka-rastochka-i-honingovanie-blokov-cilindrov-remont&id=27730086&sessionGeoId=340" target="_blank">в нашем объявлении
                    <br>на DROM.ru</a> Добро пожаловать!
                    <br>Ждем вопросов и комментариев!</strong>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="addquestion" id="captcha">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-12">
                <p id="addcomment"><span>Добавить комментарий</span><a href=""></a></p>
                Ваш e-mail не будет опубликован. Обязательные поля помечены<red>*</red>
                <form action="/postAddAnswer" method="POST" id="ajax-form">
                  {{ csrf_field() }}
                   <input type="text" id="question" hidden="hidden" name="question">
                    <label>Комментарий</label>
                    <textarea name="comment" id="" placeholder="Комментарий" required></textarea>
                    <label>Имя<red>*</red></label>
                    <input name="name" type="text" placeholder="Имя" maxlength="20" required>
                    <label>E-mail<red>*</red></label>
                    <input name="email" type="email" placeholder="E-mail" maxlength="30" required>
                    <div class="captcha">
                        <span class="refresh_captcha">{!! captcha_img() !!}</span>
                        <label for="">Код с картинки<red>*</red></label>
                        <input name="captcha" type="text" placeholder="Введите код с картинки*" required>
                    </div>
                    <label id="captchaerror" class="text-danger" style="display:none">*Ошибка - повторите ввод капчи</label><br>
                    <button type="submit">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="/js/refresh_cap.js"></script>
@endsection
