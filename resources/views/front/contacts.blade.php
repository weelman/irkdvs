@extends('layouts.front')
@section('meta')
<meta name="description" content="">
<meta name=«title» content="">
<meta property="og:locale" content="ru_RU">
<title>Контакты</title>
<link rel="stylesheet" href="css/index.css">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
<script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 17, // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(52.283202, 104.277974), // New York
                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                }
            ]
            };
            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(52.283202, 104.277974)
                , map: map
                , icon: {
                    url: "/img/logos/mapmarket.png"
                    , scaledSize: new google.maps.Size(72, 72)
                }
            });
        }
    </script>
@endsection

@section('frontcontent')
<div class="contact">
    <div class="container">
        <div class="row">
            <div class="col-12 contact_first w-100">
                <div class="row m-0 justify-content-md-center">
                    <p>Мастерская по ремонту и восстановлению деталей ДВС.
                    <br>Ремонт головок блока, ремонт блоков цилиндров,
                    <br>ремонт шатунов, ремонт, балансировка и восстановление
                    <br>карданных валов, ремонт постели коленвала и распредвала,
                    <br>обработка тормозных дисков и барабанов, обработка маховиков.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-12">
                <h1>Контактная информация:</h1>
                <div class="contact_contain contact_first h-100">
                    <p>Тел:
                    <br><a href="tel:83952675757">8(3952) 67-57-57,</a>
                    <br><a href="tel:83952671514">8(3952) 67-15-14,</a></p>
                    <br>
                    <p>Доставка деталей в Иркутске:
                    <br><a href="tel:83952601011">8(3952) 60-10-11</a></p>
                    <br>
                    <p>e-mail: <a href="mailto:irkdvs@mail.ru">irkdvs@mail.ru</a></p>
                    <p>instagram: <a href="https://www.instagram.com/irk_dvs/" target="_blank">irk_dvs</a></p>
                    <p>youtube: <a href="https://www.youtube.com/channel/UCWP8DBxh3nESQ1lIaiLr5KQ" target="_blank">irk_dvs</a></p>
                </div>
            </div>
            <div class="col-lg-7 col-12">
                <h2>Наши партнеры:</h2>
                <div class="contact_contain contact_second h-100">
                    <p>1  Компания «Механика» г. Москва
                        <br><a href="https://www.mehanika.ru/" target="_blank">https://www.mehanika.ru/</a>  <a href="https://www.motorzona.ru/" target="_blank">https://www.motorzona.ru/</a>
                        <br>2  OOO «Евромотор» г. Новочеркасск   <a href="http://eu-motor.ru" target="_blank">http://eu-motor.ru</a>
                        <br>3  Компания «Механика»  г. Санкт-Петербург <a href="https://spb.mehanika.ru/" target="_blank">https://spb.mehanika.ru/</a>
                        <br>4  ООО «Автокор» г. Омск  <a href="https://www.autokor-omsk.ru" target="_blank">https://www.autokor-omsk.ru</a>
                        <br>5  СП «ВМС»  Украина  <a href="http://promotor.ua" target="_blank">http://promotor.ua</a>
                        <br>6  Мотор — Центр 4 Такта г. Воронеж <a href="http://www.4tacta.ru/" target="_blank">http://www.4tacta.ru/</a>
                        <br>7  Компания «Моторсервис» г. Алматы  Казахстан  <a href="http://motorservice.kz" target="_blank">http://motorservice.kz</a>
                        <br>8  ООО «Контур» г. Владивосток  <a href="http://www.zx330.ru" target="_blank">http://www.zx330.ru</a>
                        <br>9  ООО «Восточная Техника «г. Иркутск  <a href="https://www.vost-tech.ru/" target="_blank">https://www.vost-tech.ru/</a>
                        <br>10 ГК «ТРУД» г. Иркутск  <a href="http://www.gktrud.ru/" target="_blank">http://www.gktrud.ru/</a></p>
                </div>
            </div>
        </div>
    </div>
    <div id="map_place">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>МЫ НА КАРТЕ</h2>
                    <address>г. Иркутск, ул. Байкальская 239, корп. 26 <br>
                    (Теплая стоянка Релейного завода)</address>
                </div>
            </div>
        </div>
        <div id="map"></div>
    </div>
</div>
@endsection
