@extends('layouts.front') @section('meta')
<meta name="description" content="">
<meta name=«title» content="">
<meta property="og:locale" content="ru_RU">
<title>Наши работы</title>
<link rel="stylesheet" href="/css/works.css">@endsection @section('frontcontent')
<div class="container works gallery">
    <div class="row">
        <div class="col-12">
            <h1>Наши работы:</h1>
        </div>
        <div class="col-lg-9 col-12">
            @foreach($works as $work)
            <p>{{ $work->title }}</p>
            <span class="text-secondary">{{ $work->description }}</span>
            <div class="row">
                @foreach($work->photos as $photo)
                <div class="col-lg-4 col-6"><img class="w-100" src="{{ $photo->url }}" alt=""></div>
                @endforeach
            </div>
            @endforeach
        </div>
        <div class="col-lg-3 col-12 d-lg-block d-none">
            <div class="go-blog text-center">
                <img src="img/logos/logo_blue.png" alt="">
                <h3>НАШ АВТОРСКИЙ
                <br>БЛОГ О РЕМОНТЕ</h3>
                <a href="/blog"><button>ЧИТАТЬ БЛОГ</button></a>
            </div>
            <div class="blog_item soc_network">
                <p>ПОДПИШИСЬ НА НАС:</p>
                <div class="blog_in">
                    <div class="row justify-content-between m-0">
                        <a href="https://www.drive2.ru/users/irkdvs/#blog">
                            <img src="/img/icons/drive_black.jpg" alt="">
                            <span>DRIVE 2</span>
                        </a>
                        <a href="">
                            <img src="/img/icons/youtube_black.jpg" alt="">
                            <span>YOUTUBE</span>
                        </a>
                        <a href="https://www.instagram.com/irk_dvs/" target="_blank">
                            <img src="/img/icons/instagram_black.jpg" alt="">
                            <span>INSTAGRAM</span>
                        </a>
                    </div>
                    <strong>Внимание!
                    <br>Посмотреть наши работы
                    <br><a href="https://forums.drom.ru/irkutsk/t1152101117.html" target="_blank">можно в нашей теме на DROM.ru,</a>
                    <br>а задать интересующие Вас
                    <br>вопросы - <a href="https://baza.drom.ru/irkutsk/service/repair/remont-golovok-bloka-gbc-rastochka-i-honingovanie-bloka-stanki-s-chpu-27730086.html?action=view&keywords=remont-golovok-bloka-rastochka-i-honingovanie-blokov-cilindrov-remont&id=27730086&sessionGeoId=340" target="_blank">в нашем объявлении
                    <br>на DROM.ru</a> Добро пожаловать!
                    <br>Ждем вопросов и комментариев!</strong>
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>
</div> @endsection
