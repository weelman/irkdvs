@extends('layouts.back')
@section('backcontent')
<?php $title = 'Одобрение комментария' ?>
<br>
<h1>Одобрить и ответить на комментарий</h1>
<br>
<form class="form-inline" method="post" action="/admin/question/Post/{{ $question->id }}">
 {{ csrf_field() }}
  <div class="form-group mb-2">
    <label for="" class="sr-only">Имя</label>
    <input type="text" class="form-control-plaintext" name="name" value="admin" required>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <label class="sr-only">Комментарий</label>
    <textarea name="comment" class="form-control" cols="40" rows="3" required></textarea>
  </div>
  <button type="submit" class="btn btn-primary mb-2">@if($question->status == '2')Опубликовать с ответом@elseОтветить@endif</button>
  @if($question->status == '2')<a href="/admin/question/nocomment/{{ $question->id }}"><button type="button" class="btn btn-warning mb-2 ml-3">Опубликовать без ответа</button></a>@endif
</form>
<table class="table table-hover some_table">
    <tbody>
        <tr>
            <th>Дата</th>
            <th>Email</th>
            <th>Имя</th>
            <th>Комментарий</th>
            <th></th>
        </tr>
        <tr class="table">
            <td colspan="5">Вопрос:</td>
        </tr>
        <tr class="table table-{{ $question->getColor() }}">
            <td>{{ $question->dates() }}</td>
            <td>{{ $question->email }}</td>
            <td>{{ $question->name }}</td>
            <td>{{ $question->comment }}</td>
            <td></td>
        </tr>
        <tr class="table text">
            <td colspan="5">Ответы:</td>
        </tr>
        @foreach( $question->answers as $answer)
            <tr class="table table-{{ $answer->getColor() }}" style="vertical-align: middle">
                <td>{{ $answer->dates() }}</td>
                <td>{{ $answer->email }}</td>
                <td>{{ $answer->name }}</td>
                <td>{{ $answer->comment }}</td>
                <td>@if($answer->status == '2') <button class="btn btn-warning answPubl" data-url="/admin/answer/publAjax/{{ $answer->id }}">Опубликовать ответ</button> @endif <button class="btn btn-danger answDel" data-url="/admin/answer/deleteAjax/{{ $answer->id }}">Удалить</button></td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
@section('javascript')
<script>
    $('.answPubl').click(function(){
        if(!window.confirm("Точно опубликовать ответ?"))
            return false;
        var but = $(this);
        var url = $(this).data('url');
          $.ajax({
             type: 'get',
             url: url,
             cache: false,

             success: function () {
                 but.parent().parent().removeClass('table-warning').addClass('table-success');
                 but.remove();
                 $('.badge-answer').text($('.badge-answer').text()-1)
             },

             error: function()
             {
                 but.parent().parent().removeClass('table-warning').addClass('table-success');
                 but.remove();
                 $('.badge-answer').text($('.badge-answer').text()-1)
             }
        });
    });

    $('.answDel').click(function(){
        if(!window.confirm("Точно удалить?"))
            return false;
        var but = $(this);
        var url = $(this).data('url');
          $.ajax({
             type: 'get',
             url: url,
             cache: false,

             success: function () {
                 but.parent().parent().remove();
                 $('.badge-answer').text($('.badge-answer').text()-1)
             },

             error: function()
             {
                 but.parent().parent().remove();
                 $('.badge-answer').text($('.badge-answer').text()-1)
             }
        });
    });
</script>
@endsection
