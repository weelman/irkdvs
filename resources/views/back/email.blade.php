@extends('layouts.back')
@section('backcontent')
   <?php $title = 'Список email' ?>
    <br>
    <h1>Email</h1>
    <br>
        <table class="table table-hover some_table">
            <tbody>
                <tr>
                    <th>Дата</th>
                    <th>Email</th>
                    <th>Имя</th>
                    <th>Действия</th>
                </tr>
                @foreach($emails as $email)
                <tr class="table table-success">
                    <td>{{ $email->dates() }}</td>
                    <td>{{ $email->email }}</td>
                    <td>{{ $email->name }}</td>
                    <td>
                    <button class="btn btn-danger answDel w-100" data-url="/admin/email/deleteAjax/{{ $email->id }}">Удалить</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <center>
            {{ $emails->links() }}
        </center>
    @endsection
    @section('javascript')
        <script>
            $('.answDel').click(function(){
                if(!window.confirm("Точно удалить?"))
                    return false;
                var but = $(this);
                var url = $(this).data('url');
                  $.ajax({
                     type: 'get',
                     url: url,
                     cache: false,

                     success: function () {
                         but.parent().parent().remove();
                         $('.badge-email').text($('.badge-email').text()-1)
                     },

                     error: function()
                     {
                         but.parent().parent().remove();
                         $('.badge-email').text($('.badge-email').text()-1)
                     }
                });
            });
        </script>
    @endsection
