@extends('layouts.back')
@section('backcontent')
   <?php $title = 'Ответы' ?>
    <br>
    <h1>Ответы</h1>
    <form action="/admin/answer/find">
       <div class="form-check form-check-inline">
            <select class="custom-select" name="checkbox" id="">
                <option value="all" @if ($checkbox == 'all') selected @endif>Все</option>
                <option value="success" @if ($checkbox == 'success') selected @endif>Одобренные</option>
                <option value="moder" @if ($checkbox == 'moder') selected @endif>На модерации</option>
            </select>
        </div>
        <div class="form-check form-check-inline">
          <button class="btn btn-info">Показать</button>
        </div>
    </form>
    <br>
        <table class="table table-hover some_table">
            <tbody>
                <tr>
                    <th>Дата</th>
                    <th>Email</th>
                    <th>Имя</th>
                    <th>Ответ</th>
                    <th>Вопрос</th>
                    <th>Действия</th>
                </tr>
                @foreach($answer_moder as $answer)
                <tr class="table-{{$answer->getColor()}}">
                    <td>{{ $answer->dates() }}</td>
                    <td>{{ $answer->email }}</td>
                    <td>{{ $answer->name }}</td>
                    <td>{{ $answer->comment }}</td>
                    <td>{{ $answer->question()->first()->comment }}</td>
                    <td>@if($answer->status=='2')<button class="btn btn-success answPubl mb-2 w-100" data-url="/admin/answer/publAjax/{{ $answer->id }}">Опубликовать</button>@endif
                    <button class="btn btn-danger answDel w-100" data-url="/admin/answer/deleteAjax/{{ $answer->id }}">Удалить</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <center>
            {{ $answer_moder->links() }}
        </center>
    @endsection
    @section('javascript')
        <script>
            $('.answPubl').click(function(){
                if(!window.confirm("Точно опубликовать ответ?"))
                    return false;
                var but = $(this);
                var url = $(this).data('url');
                  $.ajax({
                     type: 'get',
                     url: url,
                     cache: false,

                     success: function () {
                         but.parent().parent().removeClass('table-warning').addClass('table-success');
                         but.remove();
                         $('.badge-answer').text($('.badge-answer').text()-1)
                     },

                     error: function()
                     {
                         but.parent().parent().removeClass('table-warning').addClass('table-success');
                         but.remove();
                         $('.badge-answer').text($('.badge-answer').text()-1)
                     }
                });
            });
            $('.answDel').click(function(){
                if(!window.confirm("Точно удалить?"))
                    return false;
                var but = $(this);
                var url = $(this).data('url');
                  $.ajax({
                     type: 'get',
                     url: url,
                     cache: false,

                     success: function () {
                         but.parent().parent().remove();
                         $('.badge-answer').text($('.badge-answer').text()-1)
                     },

                     error: function()
                     {
                         but.parent().parent().remove();
                         $('.badge-answer').text($('.badge-answer').text()-1)
                     }
                });
            });
        </script>
    @endsection
