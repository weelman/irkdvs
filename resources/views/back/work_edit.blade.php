@extends('layouts.back')
@section('backcontent')
   <?php $title = 'Изменить работу' ?>
    <br>
    <h1>Изменить работу</h1>
    @if(Session::has('ses'))
       <div class="alert alert-info alert-dismissible fade show" role="alert">
           <strong>{{ Session('ses') }}</strong>
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
       </div>
       @endif
    <div class="col-8">
        <div class="col-12 p-0">
            <div class="row my-4 allphoto">
               @foreach($work->photos as $key=>$photo)
                <div class="col-2 text-center photon-{{$key}}">
                    <img class="w-100" src="{{$photo->url}}" alt="">
                    <p>№{{ $key+1 }}</p>
                </div>
                @endforeach
            </div>
        </div>
        @if(count($work->photos)>0)
        <form action="/admin/works/changePhotoPost/{{ $work->id }}" method="post"  enctype="multipart/form-data" class="mb-5">
          {{ csrf_field() }}
          <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">Фото под замену</span>
          </div>
          <input type="file" class="form-control" name="photo" accept="image/jpeg,image/png,image/gif" required>
        </div>

           <label class="text-info">Выберите фото над которым хотели бы произвести действие, а после выберите действие</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <button class="btn btn-info change-photo" type="submit">Заменить фото</button>
                <button class="btn btn-danger button-delete" type="button" name="delete">Удалить</button>
              </div>
              <select class="custom-select" name="number">
<!--                <option selected>-</option>-->
                @foreach($work->photos as $key=>$photo)
                    <option value="{{ $photo->id }}">{{ $key+1 }}</option>
                @endforeach
              </select>
            </div>
        </form>
        @endif
        <form action="/admin/works/editPost/{{ $work->id }}" method="post" enctype="multipart/form-data">
           {{ csrf_field() }}
            <div class="form-group">
                <label>Заголовок</label>
                <input type="text" class="form-control" name="title" value="{{ $work->title }}"> </div>
            <div class="form-group">
                <label>Пояснение</label>
                <textarea class="form-control" name="description" rows="3">{{ $work->description }}</textarea>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input input-change-photo" name="pic[]" multiple>
                <label class="custom-file-label">Добавить файлы</label>
            </div>
            <button type="submit" class="btn btn-success mt-3 change-all">Изменить</button>
        </form>
    </div>
@endsection
@section('javascript')
    <script>
        $('.button-delete').click(function(){
            if(!confirm('Удалить фото?'))
                return false;
            console.log($('.custom-select').val());
            var option = $('select option:selected');
            console.log(option);
            var numbphoto = option.text();
            console.log(numbphoto);
            var url = '/admin/works/deletePhoto/'+ $('.custom-select').val();
            $.ajax({
                type: 'get',
                url: url,
                cache: false,

                 success: function () {
                     $(option).remove();
                     $('.photon-' + (numbphoto-1)).remove();
                     if($('option').length==0)
                     {
                         location.reload();
                     }
                 },

                 error: function()
                 {
                     alert('Возникла ошибка! Попробуйте перезагрузить страницу');
                 }
            });
        });
        $(function(){
            $(".change-all").click(function(){
                var $fileUpload = $(".input-change-photo");
                if (parseInt($fileUpload.get(0).files.length) > (6-$('option').length)){
                    alert("Одна работа не может иметь более 6 фото (ограничение системы)");
                    return false;
                }
            });
        });
    </script>
@endsection
