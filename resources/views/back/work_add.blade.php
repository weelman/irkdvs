@extends('layouts.back')
@section('backcontent')
   <?php $title = 'Добавить работу' ?>
    <br>
    <h1>Добавить работу</h1>
    <div class="col-8">
        <form action="/admin/works/addPost" method="post" enctype="multipart/form-data">
           {{ csrf_field() }}
            <div class="form-group">
                <label>Заголовок</label>
                <input type="text" class="form-control" name="title"> </div>
            <div class="form-group">
                <label>Пояснение</label>
                <textarea class="form-control" name="description" rows="3"></textarea>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="pic[]" accept="image/jpeg,image/png,image/gif" multiple>
                <label class="custom-file-label">Добавить файлы</label>
            </div>
            <button type="submit" class="btn btn-success mt-3">Добавить</button>
        </form>
    </div>
@endsection
@section('javascript')
<script>
    $(function(){
        $("button[type = 'submit']").click(function(){
           var $fileUpload = $("input[type='file']");
           if (parseInt($fileUpload.get(0).files.length) > 6){
              alert("Вы не можете загрузить более 6 файлов (ограничение системы)");
              return false;
           }
        });
     });
</script>
@endsection
