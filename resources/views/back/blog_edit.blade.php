@extends('layouts.back')
@section('backcontent')
    <?php $title = 'Изменить статью' ?>
    <br>
    <h1>Изменить статью</h1>
    <div class="col-8">
        <form action="/admin/blog/editPost/{{ $blog->id }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Заголовок</label>
                <input type="text" class="form-control" name="title" value="{{ $blog->title }}" required> </div>

            <div class="form-group">
                <label>Время</label>
                <input class="form-control" name="created_at" value="{{ $blog->created_at }}" required>
            </div>
            <div class="form-group">
                <label>Пояснение</label>
                <textarea class="form-control" name="description" rows="3" required>{{ str_replace( "<br />", "", $blog->description) }}</textarea>
            </div>
            <div class="custom-file mb-4">
                <input type="file" class="custom-file-input" name="pic" accept="image/jpeg,image/png,image/gif">
                <label class="custom-file-label">Изменить обложку</label>
                <p class="text-info">**Оставьте пустым, если не желаете менять</p>
            </div>
            @foreach($blog->elements as $key=>$item)
                @if($item->type==0)
                    <div class="row my-3 align-items-center">
                        <div class="col-3">
                            <img class="w-100" src="{{ $item->content }}" alt="">
                        </div>
                        <div class="col-9">
                            <div class="input-group mt-2">
                                <div class="custom-file">
                                    <input class="element-{{ $key }}" type="text" value="{{ $item->content }}" hidden="hidden" name="elem[{{ $key }}]">
                                    <input type="file" class="custom-file-input elem-go element-{{ $key }}" name="elem[{{ $key }}]" accept="image/jpeg,image/png,image/gif">
                                    <label class="custom-file-label">Изменить фото</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-danger mini-delete big-delete button-{{ $key }}" data-count="{{ $key }}" type="button">Удалить</button>
                                </div>
                            </div>
                            <p class="text-info">**Оставьте пустым, если не желаете менять</p>
                        </div>
                    </div>
                @else
                    <div>
                        <div class="input-group mt-2">
                            <textarea name="elem[{{ $key }}]" rows="3"
                                      class="form-control elem-go element-{{ $key }}" required>{{ str_replace( "<br />", "", $item->content) }}</textarea>
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-danger mini-delete button-{{ $key }}" type="button" data-count="{{ $key }}">Удалить</button>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="input-group my-4 adder">
                <div class="input-group-prepend">
                    <button class="btn btn-outline-info" type="button">Вставить</button>
                </div>
                <select class="custom-select">
                    <option value="1">Текст</option>
                    <option value="2">Фото</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success my-3">Изменить</button>
            <a href="/admin/blog" class="btn btn-danger my-3">Отменить изменения</a>
        </form>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function(){
            var count = $('.elem-go').length;
            $('.adder button').click(function () {
                var whatadd = $('.adder select option:selected').val();
                if(whatadd == 2)
                {
                    $('.adder').before('' +
                        '<div><div class="input-group mt-2">' +
                        '<div class="custom-file">\n' +
                        '<input type="file" class="custom-file-input elem-go element-' + count + '" name="elem[' + count + ']" accept="image/jpeg,image/png,image/gif" required>\n' +
                        '<label class="custom-file-label">Добавить фото</label>\n' +
                        '</div>' +
                        '<div class="input-group-append">\n' +
                        '<button class="btn btn-outline-danger mini-delete button-' + count + '" data-count="' + count + '" type="button">Удалить</button>\n' +
                        '</div>' +
                        '</div></div>');
                } else {
                    $('.adder').before('' +
                        '<div><div class="input-group mt-2">\n' +
                        '<textarea class="form-control elem-go element-' + count + '" name="elem[' + count + ']" rows="3" required></textarea>\n' +
                        '<div class="input-group-prepend">\n' +
                        '<button class="btn btn-outline-danger mini-delete button-' + count + '" data-count="' + count + '" type="button">Удалить</button>\n' +
                        '</div>' +
                        '</div></div>');
                }
                count++;
            });
            $(document).on('click', '.mini-delete', function () {
                name = $(this).data('count');
                console.log(name);
                max = $('.elem-go').length-1;
                if($(this).hasClass('big-delete')) {
                    $(this).parent().parent().parent().parent().remove();
                } else {
                    $(this).parent().parent().remove();
                }
                if(name<max) {
                    name++;
                    // console.log(name +' name; ' + max + ' max;  Надо менять цифры');
                    for (name; name<=max; ++name) {
                        var newname = $(".element-" + name).parent().parent().find('.mini-delete').data('count')-1;
                        console.log(newname);
                        $(".button-" + name).data('count', newname);
                        $(".element-" + name).attr('name', 'elem[' + newname + ']');
                        $(".element-" + name).addClass('element-' + newname);
                        $(".button-" + name).addClass('button-' + newname);
                        $(".element-" + name).removeClass('element-' + name);
                        $(".button-" + name).removeClass('button-' + name);
                    }
                }
                count--;
            });
            $("input:file").change(function () {
                console.log('change');
                $(this).parent().find("input:text").remove();
                $(this).parent().find("label").text("Фото загружено! Нажмите кнопку изменить, чтобы применить изменения.");
            });
        });
    </script>
@endsection
