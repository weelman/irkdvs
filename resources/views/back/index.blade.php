@extends('layouts.back')
@section('backcontent')
    <?php $title = 'Главная страница администратора' ?>
    <br>
    <div class="col-8"><div class="alert alert-warning alert-dismissible fade show" role="alert">
        Количество непрочитанных комментариев <strong>{{ App\Question::where('status', '2')->count() }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div></div>
    <div class="col-8"><div class="alert alert-warning alert-dismissible fade show" role="alert">
        Количество непрочитанных ответов <strong>{{ App\Answer::where('status', '2')->count() }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div></div>
@endsection
