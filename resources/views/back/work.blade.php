@extends('layouts.back')
@section('backcontent')
<?php $title = 'Работы' ?>
<br>
   @if(Session::has('ses'))
   <div class="alert alert-info alert-dismissible fade show" role="alert">
       <strong>{{ Session('ses') }}</strong>
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   @endif
<h1>Работы</h1>
<p class="text-danger">**Работы будут отображаться на основном сайте только при наличии добавленного фото</p>
<a href="/admin/works/add"><button class="btn btn-success my-3">Добавить</button></a>
<br>
    <table class="table table-hover some_table">
        <tbody>
            <tr>
                <th>Дата</th>
                <th>Заголовок</th>
                <th>Описание</th>
                <th>Количество фото</th>
                <th>Действия</th>
            </tr>
            @foreach ($works as $work)
            <tr class="table @if($work->photos()->count()>0) table-success @else table-warning @endif">
                <td>{{ $work->created_at }}</td>
                <td>{{ $work->title }}</td>
                <td>{{ $work->description }}</td>
                <td>{{ $work->photos()->count() }}</td>
                <td><a href="/admin/works/edit/{{ $work->id }}"><button class="btn btn-success">Изменить</button></a>
                <button class="btn btn-danger button-delete" data-url="/admin/works/deleteAjax/{{ $work->id }}">Удалить</button></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <center>
        {{ $works->links() }}
    </center>
@endsection
@section('javascript')
<script>
    $('.button-delete').click(function(){
        if(!confirm('Вы уверены, что хотите удалить работу?'))
            return false;
        var button = $(this);
        var delurl = $(this).data('url');
        console.log(delurl);
        $.ajax({
             type: 'get',
             url: delurl,
             cache: false,

             success: function () {
                 $(button).parent().parent().remove();
             },

             error: function()
             {
                 alert('Возникла ошибка! Попробуйте перезагрузить страницу');
             }
        });
    });
</script>
@endsection
