@extends('layouts.back')
@section('backcontent')
   <?php $title = 'Комментарии' ?>
    <br>
   @if(Session::has('ses'))
   <div class="alert alert-info alert-dismissible fade show" role="alert">
       <strong>{{ Session('ses') }}</strong>
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   @endif
    <h1>Комментарии</h1>
    <form action="/admin/question/find">
       <div class="form-check form-check-inline">
            <select class="custom-select" name="checkbox" id="">
                <option value="all" @if ($checkbox == 'all') selected @endif>Все</option>
                <option value="success" @if ($checkbox == 'success') selected @endif>Одобренные</option>
                <option value="moder" @if ($checkbox == 'moder') selected @endif>На модерации</option>
            </select>
        </div>
        <div class="form-check form-check-inline">
          <button class="btn btn-info">Показать</button>
        </div>
    </form>
    <br>
        <table class="table table-hover some_table">
            <tbody>
                <tr>
                    <th>Дата</th>
                    <th>Email</th>
                    <th>Имя</th>
                    <th>Комментарий</th>
                    <th>Действия</th>
                </tr>
                @foreach($question_moder as $question)
                <tr class="table-{{ $question->getColor() }}">
                    <td>{{ $question->dates() }}</td>
                    <td>{{ $question->email }}</td>
                    <td>{{ $question->name }}</td>
                    <td>{{ $question->comment }}</td>
                    <td class="text-center"><a href="/admin/question/{{ $question->id }}"><button class="btn btn-success mb-2 w-100">Ответить</button></a>
                    <form onSubmit="if(!confirm('Вы уверены, что хотите удалить вопрос?')){return false;}"action="{{ url('/admin/question/delete/'. $question->id) }}" method="get">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <button class="btn btn-danger w-100">Удалить</button>
                    </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div style="text-align: center;">
            {{ $question_moder->links() }}
        </div>
    @endsection
