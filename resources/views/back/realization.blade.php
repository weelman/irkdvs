@extends('layouts.back')
@section('backcontent')
    <?php $title = 'Детали' ?>
    <br>
   @if(Session::has('ses'))
   <div class="alert alert-info alert-dismissible fade show" role="alert">
       <strong>{{ Session('ses') }}</strong>
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   @endif
    <h1>Детали</h1>
    <p class="text-danger">**Детали для реализации будут отображаться на основном сайте только при наличии добавленного фото</p>
    <a href="/admin/realizations/add"><button class="btn btn-success my-3">Добавить</button></a>
    <br>
    <table class="table table-hover some_table">
        <tbody>
        <tr>
            <th>Дата</th>
            <th>Заголовок</th>
            <th>Описание</th>
            <th>Гарантии</th>
            <th>Стоимость</th>
            <th>Статус</th>
            <th>Количество фото</th>
            <th>Действия</th>
        </tr>
        @foreach ($realizations as $realization)
            <tr class="table @if($realization->photos()->count()>0) table-success @else table-warning @endif">
                <td>{{ $realization->created_at }}</td>
                <td>{{ $realization->title }}</td>
                <td>{{ $realization->description }}</td>
                <td>{{ $realization->garant }}</td>
                <td>{{ $realization->price }}</td>
                <td>{{ $realization->getStatus() }}</td>
                <td>{{ $realization->photos()->count() }}</td>
                <td><a href="/admin/realizations/edit/{{ $realization->id }}"><button class="btn btn-success">Изменить</button></a>
                    <button class="btn btn-danger button-delete" data-url="/admin/realizations/deleteAjax/{{ $realization->id }}">Удалить</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="text-align: center;">
        {{ $realizations->links() }}
    </div>
@endsection
@section('javascript')
    <script>
        $('.button-delete').click(function(){
            if(!confirm('Вы уверены, что хотите удалить работу?'))
                return false;
            var button = $(this);
            var delurl = $(this).data('url');
            console.log(delurl);
            $.ajax({
                type: 'get',
                url: delurl,
                cache: false,

                success: function () {
                    $(button).parent().parent().remove();
                },

                error: function()
                {
                    alert('Возникла ошибка! Попробуйте перезагрузить страницу');
                }
            });
        });
    </script>
@endsection
