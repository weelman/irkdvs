@extends('layouts.back')
@section('backcontent')
    <?php $title = 'Добавить деталь' ?>
    <br>
    <h1>Добавить деталь</h1>
    <div class="col-8">
        <form action="/admin/realizations/addPost" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Заголовок</label>
                <input type="text" class="form-control" name="title" required> </div>
            <div class="form-group">
                <label>Пояснение</label>
                <textarea class="form-control" name="description" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <label>Гарантия</label>
                <input type="text" class="form-control" name="garant" required>
            </div>
            <div class="form-group">
                <label>Стоимость</label>
                <input type="number" class="form-control" name="price" required>
            </div>
            <div class="form-group">
                <label>Статус</label>
                <select class="custom-select" name="status">
                    <option value="0">В продаже</option>
                    <option value="1">Продано</option>
                </select>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="pic[]" accept="image/jpeg,image/png,image/gif" multiple>
                <label class="custom-file-label">Добавить файлы</label>
            </div>
            <button type="submit" class="btn btn-success mt-3">Добавить</button>
        </form>
    </div>
@endsection
@section('javascript')
    <script>
        $(function(){
            $("button[type = 'submit']").click(function(){
                var $fileUpload = $("input[type='file']");
                if (parseInt($fileUpload.get(0).files.length) > 4){
                    alert("Вы не можете загрузить более 4 файлов (ограничение системы)");
                    return false;
                }
            });
        });
    </script>
@endsection
