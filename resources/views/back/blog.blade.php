@extends('layouts.back')
@section('backcontent')
    <?php $title = 'Блог' ?>
    <br>
   @if(Session::has('ses'))
   <div class="alert alert-info alert-dismissible fade show" role="alert">
       <strong>{{ Session('ses') }}</strong>
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   @endif
    <h1>Блог</h1>
    <a href="/admin/blog/add"><button class="btn btn-success my-3">Добавить</button></a>
    <br>
    <table class="table table-hover some_table">
        <tbody>
        <tr>
            <th>Дата</th>
            <th>Заголовок</th>
            <th>Описание</th>
            <th>Доп элементы</th>
            <th>Картинка</th>
            <th>Действия</th>
        </tr>
        @foreach ($blog_items as $item)
            <tr class="table @if($item->elements()->count()>0) table-success @else table-warning @endif">
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ str_replace("<br />", " (**перенос строки**) ", str_limit($item->description, 100)) }}</td>
                <td>@if($item->elements()->count()>0) Есть @else Нет @endif</td>
                <td><img style="width: 100px" src="{{ $item->pic }}" alt=""></td>
                <td><a href="/admin/blog/edit/{{ $item->id }}"><button class="btn btn-success">Изменить</button></a>
                    <button class="btn btn-danger button-delete" data-url="/admin/blog/deleteAjax/{{ $item->id }}">Удалить</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="text-align: center;">
        {{ $blog_items->links() }}
    </div>
@endsection
@section('javascript')
    <script>
        $('.button-delete').click(function(){
            if(!confirm('Вы уверены, что хотите удалить работу?'))
                return false;
            var button = $(this);
            var delurl = $(this).data('url');
            console.log(delurl);
            $.ajax({
                type: 'get',
                url: delurl,
                cache: false,

                success: function () {
                    $(button).parent().parent().remove();
                },

                error: function()
                {
                    alert('Возникла ошибка! Попробуйте перезагрузить страницу');
                }
            });
        });
    </script>
@endsection
