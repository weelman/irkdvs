@extends('layouts.back')
@section('backcontent')
<?php $title = 'Фото' ?>
<br>
   @if(Session::has('ses'))
   <div class="alert alert-info alert-dismissible fade show" role="alert">
       <strong>{{ Session('ses') }}</strong>
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   @endif
<h1>Фото</h1>
<br><br>
    <form action="/admin/trust/post" method="post" class="w-100 row mb-3" enctype="multipart/form-data">
      {{ csrf_field() }}
       <div class="col-lg-6 col-12 text-center">
           <h3>Расположение как на сайте
           <br>Нажмите кнопку ниже, чтобы фото загрузились на сайт</h3>
        <button type="submit" class="btn btn-success mx-auto">Изменить</button>
       </div>
            <div class="col-lg-6 col-12">
                <div class="row">
                    <div class="col-6">
                        <img class="w-100" src="/img/trust/trust_1.jpg" alt="">

                        <div class="input-group mb-3">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="go[1]">
                            <label class="custom-file-label">Загрузите фото</label>
                          </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <img class="w-100" src="/img/trust/trust_2.jpg" alt="">

                        <div class="input-group mb-3">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="go[2]">
                            <label class="custom-file-label">Загрузите фото</label>
                          </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <img class="w-100" src="/img/trust/trust_3.jpg" alt="">

                        <div class="input-group mb-3">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="go[3]">
                            <label class="custom-file-label">Загрузите фото</label>
                          </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <img class="w-100" src="/img/trust/trust_4.jpg" alt="">

                        <div class="input-group mb-3">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="go[4]">
                            <label class="custom-file-label">Загрузите фото</label>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="col-3">
            <img class="w-100" src="/img/trust/trust_5.jpg" alt="">

            <div class="input-group mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="go[5]">
                <label class="custom-file-label">Загрузите фото</label>
              </div>
            </div>
        </div>
        <div class="col-3">
            <img class="w-100"src="/img/trust/trust_6.jpg" alt="">

            <div class="input-group mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="go[6]">
                <label class="custom-file-label">Загрузите фото</label>
              </div>
            </div>
        </div>
        <div class="col-3">
            <img class="w-100" src="/img/trust/trust_7.jpg" alt="">

            <div class="input-group mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="go[7]">
                <label class="custom-file-label">Загрузите фото</label>
              </div>
            </div>
        </div>
        <div class="col-3">
            <img class="w-100"src="/img/trust/trust_8.jpg" alt="">

            <div class="input-group mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="go[8]">
                <label class="custom-file-label">Загрузите фото</label>
              </div>
            </div>
        </div>
    </form>
@endsection
