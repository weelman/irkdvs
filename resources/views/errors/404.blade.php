@extends('layouts.front')
@section('meta')
<meta name="description" content="">
<meta name=«title» content="Страница не найдена | 404">
<meta property="og:locale" content="ru_RU">
<title>Страница не найдена | 404</title>
<link rel="stylesheet" href="css/works.css">
@endsection

@section('frontcontent')

    <div class="container error">
        <div class="row align-items-center">
            <div class="offset-lg-1 col-lg-6 col-12"> <span>404</span> </div>
            <div class="col-lg-5 col-12">
                <p>ТАКОЙ СТРАНИЦЫ НЕТ</p>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-7 col-lg-4"> <a href="/">ВЕРНУТЬСЯ НА ГЛАВНУЮ ></a> </div>
        </div>
    </div>
@endsection
