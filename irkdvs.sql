-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 02 2018 г., 04:38
-- Версия сервера: 10.1.30-MariaDB
-- Версия PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `irkdvs`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `name`, `email`, `comment`, `status`, `is_admin`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Гена', 'gena@gena.com', 'Будет 4', 1, 0, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL),
(2, 1, 'Лёша', 'lex@gena.com', 'Вы правы, будет 4', 1, 0, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL),
(3, 2, 'Алиса', 'alice@wonderland.com', 'Будет 6', 1, 0, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL),
(4, 2, 'Эвелин', 'eveline@gena.com', 'Вы правы, будет 6', 2, 0, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Олег', 'oleg@oleg.com', '2018-03-29 18:11:21', '2018-03-29 18:11:21'),
(2, 'Гена', 'gena@gena.com', '2018-03-29 18:11:21', '2018-03-29 18:11:21'),
(3, 'Лёша', 'lex@gena.com', '2018-03-29 18:11:21', '2018-03-29 18:11:21'),
(4, 'Боря', 'borya@oleg.com', '2018-03-29 18:11:21', '2018-03-29 18:11:21'),
(5, 'Алиса', 'alice@wonderland.com', '2018-03-29 18:11:21', '2018-03-29 18:11:21'),
(6, 'Эвелин', 'eveline@gena.com', '2018-03-29 18:11:21', '2018-03-29 18:11:21'),
(7, 'Дикобраз Петрович', 'dikobraz@dikobraz.dikobraz', '2018-03-29 18:11:21', '2018-03-29 18:11:21');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2018_03_20_095831_create_questions_table', 1),
(14, '2018_03_21_022445_create_answers_table', 1),
(15, '2018_03_21_030722_add_votes_to_answers_table', 1),
(16, '2018_03_21_034603_create_emails_table', 1),
(17, '2018_03_27_095536_create_works_table', 1),
(18, '2018_03_27_095753_create_realizations_table', 1),
(19, '2018_03_28_024124_create_photos_table', 1),
(20, '2018_03_29_044905_add_votes_to_photos_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `work_id` int(10) UNSIGNED DEFAULT NULL,
  `realization_id` int(10) UNSIGNED DEFAULT NULL,
  `type` smallint(6) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `big_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `photos`
--

INSERT INTO `photos` (`id`, `work_id`, `realization_id`, `type`, `url`, `big_url`, `created_at`, `updated_at`) VALUES
(3, 3, NULL, 0, '/img/odmen/works/Dml2v7.jpg', NULL, '2018-03-29 22:05:46', '2018-03-29 22:05:46'),
(4, 3, NULL, 0, '/img/odmen/works/PoV5h0.jpg', NULL, '2018-03-29 22:05:46', '2018-03-29 22:05:46'),
(5, 3, NULL, 0, '/img/odmen/works/4QxODe.png', NULL, '2018-03-29 22:05:46', '2018-03-29 22:05:46'),
(6, 3, NULL, 0, '/img/odmen/works/grLCHU.jpg', NULL, '2018-03-30 00:16:57', '2018-03-30 00:16:57'),
(8, 3, NULL, 0, '/img/odmen/works/x2UUkC.jpg', NULL, '2018-03-30 00:17:43', '2018-03-30 00:17:43'),
(9, 3, NULL, 0, '/img/odmen/works/06COk7.jpg', NULL, '2018-03-30 00:18:13', '2018-03-30 00:18:13'),
(11, NULL, 2, 0, '/img/odmen/realizations/kMuJ.jpg', NULL, '2018-03-31 02:40:39', '2018-03-31 02:40:39'),
(21, NULL, 2, 0, '/img/odmen/realizations/TGZY6q.jpg', NULL, '2018-03-31 23:51:10', '2018-03-31 23:51:10'),
(22, NULL, 3, 0, '/img/odmen/realizations/GgIk.jpg', NULL, '2018-04-01 00:54:46', '2018-04-01 00:54:46'),
(23, NULL, 2, 0, '/img/odmen/realizations/FmmgWD.jpg', NULL, '2018-04-01 05:24:41', '2018-04-01 05:24:41');

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `name`, `email`, `comment`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Олег', 'oleg@oleg.com', 'Сколько будет 2+2?', 1, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL),
(2, 'Боря', 'borya@oleg.com', 'Сколько будет 3+3?', 1, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL),
(3, 'Дикобраз Петрович', 'dikobraz@dikobraz.dikobraz', 'Сколько игл у дикобраза???', 2, '2018-03-29 18:11:21', '2018-03-29 18:11:21', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `realizations`
--

CREATE TABLE `realizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `garant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `realizations`
--

INSERT INTO `realizations` (`id`, `title`, `description`, `garant`, `price`, `status`, `created_at`, `updated_at`) VALUES
(2, 'test zag', 'poya', 'garan', 12321, 0, '2018-03-31 02:40:39', '2018-03-31 02:40:39'),
(3, 'sdf', 'test', 'est\\', 112323, 0, '2018-04-01 00:54:46', '2018-04-01 00:54:46');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@irkdvs.ru', '$2y$10$ZDruBMC1KPPasiF6.2Wt7uHKNYyzYFVbX/ltsbnmeWoBzO8XrxZ4u', NULL, '2018-03-29 18:11:21', '2018-03-29 18:11:21');

-- --------------------------------------------------------

--
-- Структура таблицы `works`
--

CREATE TABLE `works` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `works`
--

INSERT INTO `works` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(2, 'aye', '123', '2018-03-29 18:13:33', '2018-03-29 18:13:33'),
(3, 'test change', '123', '2018-03-29 22:05:46', '2018-03-30 00:16:36');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_question_id_foreign` (`question_id`);

--
-- Индексы таблицы `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_work_id_foreign` (`work_id`),
  ADD KEY `photos_realization_id_foreign` (`realization_id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `realizations`
--
ALTER TABLE `realizations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `realizations`
--
ALTER TABLE `realizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `works`
--
ALTER TABLE `works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);

--
-- Ограничения внешнего ключа таблицы `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_realization_id_foreign` FOREIGN KEY (`realization_id`) REFERENCES `realizations` (`id`),
  ADD CONSTRAINT `photos_work_id_foreign` FOREIGN KEY (`work_id`) REFERENCES `works` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
